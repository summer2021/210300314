import { Loading } from 'element-ui'
export function openLoading () {
  let loading = Loading.service({
    lock: true,
    text: '加载中……',
    spinner: 'el-icon-loading', // 引入的loading图标
    background: 'rgba(0, 0, 0, 0.7)'
  })
  setTimeout(function () { // 设定定时器，超时10S后自动关闭遮罩层，避免请求失败时，遮罩层一直存在的问题
    loading.close() // 关闭遮罩层
  }, 10000)
  return loading
}
