export function getIntIndex (str) {
  let i = str.length - 1
  for (; i >= 0; i--) {
    let c = str.charAt(i)
    if (c > '9' || c < '0') {
      break
    }
  }
  i++
  // 删除相同的0
  for (; i < str.length; i++) {
    if (str.charAt(i) !== 0) {
      break
    }
  }
  return i
}

export function strIsBiggerStr (str1, str2) {
  let starti = getIntIndex(str1)
  let endi = getIntIndex(str2)
  let str1String = str1.substring(0, starti)
  let str2String = str2.substring(0, endi)
  if (str1String !== str2String) {
    return false
  }
  let str1Int = parseInt(str1.substring(starti))
  let str2Int = parseInt(str2.substring(endi))
  if (str1Int === 'NaN' || str2Int === 'NaN') {
    return false
  }
  return str2Int > str1Int
}
