# 环境：

## 安装TDengine

TDengine-server-2.1.3.2-Linux-x64 

TDengine-client-2.1.3.2-Linux-x64

TDengine-client-2.1.3.2-Windows-x64

## TAOS-JDBCDriver 版本

taos-jdbcdriver-2.0.31

## jdk版本

1.8

# clone

https://gitlab.summer-ospp.ac.cn/summer2021/210300314.git

# 安装logicback

## 用idea打开logicback

![image.png](https://cdn.nlark.com/yuque/0/2021/png/2573838/1631436111448-ed258561-87a2-4b95-a4a3-a6312f490468.png)

## 导入maven的依赖

![image.png](https://cdn.nlark.com/yuque/0/2021/png/2573838/1631436265197-cace40e6-c0af-4dfb-885f-52a647b3331e.png)

## 开始运行

![image.png](https://cdn.nlark.com/yuque/0/2021/png/2573838/1631436347298-b3e25c41-a5f7-4584-ae00-d7f0fafc2112.png)

![image.png](https://cdn.nlark.com/yuque/0/2021/png/2573838/1631436413713-5eee33eb-2b4e-4e65-9b98-8c33821b9819.png)
