package com.tdengine.logicback;

import com.alibaba.druid.pool.DruidDataSource;
import com.tdengine.logicback.model.CmdPairModel;
import com.tdengine.logicback.utils.CmdEnum;
import com.tdengine.logicback.exception.ReadConfigException;
import org.junit.jupiter.api.Test;

import java.sql.*;
import java.util.*;

public class TestTdegineJDBC {
    /**
     *测试conn
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    @Test
    public void testConn() throws ClassNotFoundException, SQLException {
        Class.forName("com.taosdata.jdbc.rs.RestfulDriver");
        String jdbcUrl = "jdbc:TAOS-RS://127.0.0.1:6041/test?user=root&password=taosdata";
        Connection conn = DriverManager.getConnection(jdbcUrl);
        System.out.println("test one");
    }

    /**
     * 测试建库建表
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    @Test
    public void testCreate() throws SQLException, ClassNotFoundException {
        Class.forName("com.taosdata.jdbc.rs.RestfulDriver");
        String jdbcUrl = "jdbc:TAOS-RS://192.168.73.128:6041/test?user=root&password=taosdata";
        Connection conn = DriverManager.getConnection(jdbcUrl);
        Statement stmt = conn.createStatement();
        // create database
        stmt.executeUpdate("create database if not exists db");
        // use database
        stmt.executeUpdate("use db");
        // create table
        stmt.executeUpdate("create table if not exists tb (ts timestamp, temperature int, humidity float)");
    }

    /**
     * 测试查询
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    @Test
    public void testInsert() throws SQLException, ClassNotFoundException {
        Class.forName("com.taosdata.jdbc.rs.RestfulDriver");
        String jdbcUrl = "jdbc:TAOS-RS://192.168.73.128:6041/db?user=root&password=taosdata";
        Connection conn = DriverManager.getConnection(jdbcUrl);
        Statement stmt = conn.createStatement();
        // create table
        stmt.executeUpdate("create table if not exists tb (ts timestamp, temperature int, humidity float)");
        // insert data
        int affectedRows = stmt.executeUpdate("insert into tb values(now, 23, 10.3) (now + 1s, 20, 9.3)");
        System.out.println("insert " + affectedRows + " rows.");
    }

    /**
     * 测试查询
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    @Test
    public void testSelect() throws SQLException, ClassNotFoundException {
        Class.forName("com.taosdata.jdbc.rs.RestfulDriver");
        String jdbcUrl = "jdbc:TAOS-RS://192.168.73.128:6041/test?user=root&password=taosdata";
        Connection conn = DriverManager.getConnection(jdbcUrl);
        Statement stmt = conn.createStatement();
        ResultSet resultSet = stmt.executeQuery("show create table t1");
        while(resultSet.next()){
            System.out.println(resultSet);
            System.out.println(resultSet.getString(2));
        }
    }
    @Test
    public void testSelectData() throws SQLException, ClassNotFoundException {
        Class.forName("com.taosdata.jdbc.rs.RestfulDriver");
        String jdbcUrl = "jdbc:TAOS-RS://192.168.73.128:6041/test?user=root&password=taosdata";
        Connection conn = DriverManager.getConnection(jdbcUrl);
        Statement stmt = conn.createStatement();
         stmt.execute("INSERT INTO t1 (col0,ts, col1) VALUES (10,'2021-07-13 14:06:33.196',5);");
    }
    @Test
    public void testDruid() throws SQLException {
        DruidDataSource dataSource = new DruidDataSource();
        // jdbc properties
        dataSource.setDriverClassName("com.taosdata.jdbc.TSDBDriver");
        dataSource.setUrl("jdbc:TAOS://192.168.73.128:6030/test");
        dataSource.setUsername("root");
        dataSource.setPassword("taosdata");
        // pool configurations
        dataSource.setInitialSize(10);
        dataSource.setMinIdle(10);
        dataSource.setMaxActive(10);
        dataSource.setMaxWait(30000);
        dataSource.setValidationQuery("select server_status()");
        Connection  connection = dataSource.getConnection(); // get connection
        Statement statement = connection.createStatement(); // get statement
        //query or insert
        // ...

        connection.close(); // put back to conneciton pool
    }
    @Test
    public void testJdbc_jni() throws ClassNotFoundException, SQLException {
        Class.forName("com.taosdata.jdbc.TSDBDriver");
        String jdbcUrl = "jdbc:TAOS://192.168.73.128:6030?user=root&password=taosdata";
        Connection conn = DriverManager.getConnection(jdbcUrl);
    }
    @Test
    public void readStrPair()
    {
        String str="-t ";
        if(str.charAt(0)!='-')
        {
            return;
        }
        int startIndex=1;
        for(;startIndex<str.length()-1;startIndex++)
        {
            if(str.charAt(startIndex)==' ')
            {
                break;
            }
        }
        CmdPairModel cmdPairModel=new CmdPairModel();
        cmdPairModel.setCmd(str.substring(1,startIndex));
        cmdPairModel.setValue(str.substring(startIndex+1));
        System.out.println(cmdPairModel.getCmd());
        System.out.println(cmdPairModel.getValue());

    }
    @Test
    public void splitString()
    {
        String str="-t 63562 -p dddsdd-s ddd d dsdf dfdf  dfd dfdfd -k";
        List<String> list=new ArrayList<>();
        str=str.trim();
        int startIndex=0,endIndex=1;
        for(;startIndex<str.length();startIndex=endIndex-1)
        {
            for(;endIndex<str.length();endIndex++)
            {
                if(str.charAt(endIndex)=='-')
                {
                    break;
                }
            }
            if(startIndex<endIndex)
            {
                list.add(str.substring(startIndex,endIndex));
            }else {
                break;
            }
            endIndex++;
        }
        System.out.println(str);
        for(String s:list)
        {
            System.out.println(s);
        }
    }
    @Test
    public void testEnum()
    {
        String s="t5-t7(1,2,3)";
        int i=s.indexOf('(');
        String tablesString=s.substring(0,i);
        String columnsString=s.substring(i+1,s.length()-1);
        System.out.println(tablesString);
        System.out.println(columnsString);

    }
    @Test
    public void testTable()
    {
        String s="t1,t2,t3,t4(c1,c2),[t1-t10,t1,t2](c1-c22)";
        int startIndex=0;
        int endIndex=0;
        List<String> list=new ArrayList<>();
        /**
         * 匹配符合[
         */
        boolean bagFlag1=true;
        /**
         * 匹配符号(
         */
        boolean bagFlag2=true;
        for(;endIndex<s.length();endIndex++)
        {
            if(bagFlag1&&bagFlag2)
            {
                if(s.charAt(endIndex)==',')
                {
                    String temp=s.substring(startIndex,endIndex);
                    System.out.println(temp);
                    startIndex=endIndex+1;
                }
            }
            switch (s.charAt(endIndex))
            {
                case '[':bagFlag1=!bagFlag1;break;
                case ']':bagFlag1=!bagFlag1;break;
                case '(':bagFlag2=!bagFlag2;break;
                case ')':bagFlag2=!bagFlag2;break;
                default:break;
            }
        }
        System.out.println(s.substring(startIndex));
    }
    @Test
    void testTableRange()
    {
        String s="t1-t100r";
        String[] temp=s.split("-");
        if(temp.length!=2)
        {
            throw new ReadConfigException(CmdEnum.TABLE,s,"table的范围选择错误");
        }
        System.out.println(temp[0]);
        int i=0;
        for(;i<temp[0].length()-1&&i<temp[1].length()-1;i++)
        {
            if (temp[1].charAt(i) != temp[0].charAt(i)) {
                break;
            }
        }
        System.out.println(i);
        String sameString=temp[0].substring(0,i);
        try {
            int start= Integer.parseInt(temp[0].substring(i));
            int end= Integer.parseInt(temp[1].substring(i));
        }catch (Exception ignored)
        {
            throw new ReadConfigException(CmdEnum.TABLE,s,"");
        }

        System.out.println(sameString);
    }
    @Test
    public void testHash()
    {
        List<String> hashSet=new ArrayList<>();
        for(int i=0;i<5;i++)
        {
            hashSet.add("ddd"+i);
        }
        String string=hashSet.toString();
        string=string.substring(1,string.length()-1);
        System.out.println('('+string+')');
    }
    @Test
    void testInput()
    {
        // 放在要检测的代码段前，取开始前的时间戳
        Long startTime = System.currentTimeMillis();

        HashSet<String> hashSet=new HashSet<>();
        for(int i=0;i<Integer.MAX_VALUE;i++)
        {
            hashSet.add("123");
        }
// 放在要检测的代码段后，取结束后的时间戳
        Long endTime = System.currentTimeMillis();

// 计算并打印耗时
        Long tempTime = (endTime - startTime);
        System.out.println("花费时间："+
                (((tempTime/86400000)>0)?((tempTime/86400000)+"d"):"")+
                ((((tempTime/86400000)>0)||((tempTime%86400000/3600000)>0))?((tempTime%86400000/3600000)+"h"):(""))+
                ((((tempTime/3600000)>0)||((tempTime%3600000/60000)>0))?((tempTime%3600000/60000)+"m"):(""))+
                ((((tempTime/60000)>0)||((tempTime%60000/1000)>0))?((tempTime%60000/1000)+"s"):(""))+
                ((tempTime%1000)+"ms"));

        // 放在要检测的代码段前，取开始前的时间戳
         startTime = System.currentTimeMillis();

         hashSet=new HashSet<>();
        for(int i=0;i<Integer.MAX_VALUE;i++)
        {
            if(!hashSet.contains("123"))
            {
                hashSet.add("123");
            }
        }
// 放在要检测的代码段后，取结束后的时间戳
       endTime = System.currentTimeMillis();

// 计算并打印耗时
        tempTime = (endTime - startTime);
        System.out.println("花费时间："+
                (((tempTime/86400000)>0)?((tempTime/86400000)+"d"):"")+
                ((((tempTime/86400000)>0)||((tempTime%86400000/3600000)>0))?((tempTime%86400000/3600000)+"h"):(""))+
                ((((tempTime/3600000)>0)||((tempTime%3600000/60000)>0))?((tempTime%3600000/60000)+"m"):(""))+
                ((((tempTime/60000)>0)||((tempTime%60000/1000)>0))?((tempTime%60000/1000)+"s"):(""))+
                ((tempTime%1000)+"ms"));
    }




}
