package com.tdengine.logicback;

import com.alibaba.druid.pool.DruidDataSource;
import com.tdengine.logicback.datasource.DataSourceConnectionFactory;
import com.tdengine.logicback.model.BackConfigModel.MainConfigModel;
import com.tdengine.logicback.model.CmdPairModel;
import com.tdengine.logicback.model.DataSourceModel;
import com.tdengine.logicback.service.IsRightConfigService;
import com.tdengine.logicback.service.ReadConfigService;
import com.tdengine.logicback.service.ShowTDengineService;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.Future;

@SpringBootTest
class LogicbackApplicationTests {
    @Autowired
    DataSourceModel dataSourceModel;
    @Autowired
    ShowTDengineService showTDengineService;
    @Autowired
    MainConfigModel mainConfigModel;
    @Autowired
    IsRightConfigService isRightConfigService;
    @Autowired
    ReadConfigService readConfigService;

    @Test
    void contextLoads() {
        System.out.println(dataSourceModel.getDriverClassName());
    }

    @Test
    public void testDruid3()  {

        DruidDataSource dataSource = new DruidDataSource();
        dataSourceModel.setUrl("jdbc:TAOS://192.168.73.128:6030");
        dataSourceModel.setUsername("root");
        dataSourceModel.setPassword("taosdata");
        // jdbc properties
        dataSource.setDriverClassName(dataSourceModel.getDriverClassName());
        dataSource.setUrl(dataSourceModel.getUrl());
        dataSource.setUsername(dataSourceModel.getUsername());
        dataSource.setPassword(dataSourceModel.getPassword());
        // pool configurations
        dataSource.setInitialSize(dataSourceModel.getInitialSize());
        dataSource.setMinIdle(dataSourceModel.getMinIdle());
        dataSource.setMaxActive(dataSourceModel.getMaxActive());
        dataSource.setMaxWait(dataSourceModel.getMaxWait());
        dataSource.setValidationQuery(dataSourceModel.getValidationQuery());
        Connection connection = null; // get connection
        try {
            connection = dataSource.getConnection();
            Statement statement = connection.createStatement(); // get statement
            ResultSet resultSet = statement.executeQuery("show databases;");
            while (resultSet.next()) {
            }
            connection.close(); // put back to conneciton pool
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }
    @Test
    public void testConnection()
    {
        dataSourceModel.setUrl("jdbc:TAOS://192.168.703.128:6030");
        dataSourceModel.setUsername("root");
        dataSourceModel.setPassword("taosdata");

    }
    @Test
    public void testChangeStrToBean()
    {
        String command="-u jdbc:TAOS://192.168.73.128:6030";


    }
    @Test
    public void readConfig(){
        String str="-u root -p tdengine -t hello";
        List<String> list=readConfigService.splictString(str);
        for (String s:list) {
            CmdPairModel cmdPairModel=readConfigService.readCatalog(s);
            System.out.println(cmdPairModel.getCmd());
            System.out.println(s);
        }
    }
    @Autowired
    MainApp mainApp;
    @Test
    public void testCmdEnum()
    {
        mainApp.readConfig();
    }
    @SneakyThrows
    @Test
    public void testHashSetSplit() throws InterruptedException {

    }
    @Test
    public void test1()
    {

    }

}
