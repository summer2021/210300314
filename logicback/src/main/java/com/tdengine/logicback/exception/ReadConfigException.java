package com.tdengine.logicback.exception;

import com.tdengine.logicback.utils.CmdEnum;

/**
 * 多数情况下，创建自定义异常需要继承Exception，本例继承Exception的子类RuntimeException
 * @author Mahc
 *
 */
public class ReadConfigException extends RuntimeException {

	/**
	 * 错误信息
	 */
	private String errMes ;
	/**
	 * 异常对应的描述信息
	 */
	private String msgDes;

	private CmdEnum cmdEnum;

	public ReadConfigException() {
		super();
	}

	public ReadConfigException(String message) {
		super(message);
		msgDes = message;
	}

	public ReadConfigException(CmdEnum cmdEnum,String errMes, String msgDes) {
		super();
		this.cmdEnum=cmdEnum;
		this.errMes = errMes;
		this.msgDes = msgDes;
	}

    public ReadConfigException(CmdEnum cmdEnum) {
		super();
		this.cmdEnum=cmdEnum;
    }

    public String geterrMes() {
		return errMes;
	}

	public String getMsgDes() {
		return msgDes;
	}

	public String getErrMes() {
		return errMes;
	}

	public void setErrMes(String errMes) {
		this.errMes = errMes;
	}

	public void setMsgDes(String msgDes) {
		this.msgDes = msgDes;
	}

	public CmdEnum getCmdEnum() {
		return cmdEnum;
	}

	public void setCmdEnum(CmdEnum cmdEnum) {
		this.cmdEnum = cmdEnum;
	}
}
