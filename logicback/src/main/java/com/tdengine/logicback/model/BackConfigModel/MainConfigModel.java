package com.tdengine.logicback.model.BackConfigModel;

import com.alibaba.druid.pool.DruidDataSource;
import com.tdengine.logicback.datasource.DataSourceConnectionFactory;
import com.tdengine.logicback.model.DataSourceModel;
import com.tdengine.logicback.utils.BackFileTypeEnum;
import com.tdengine.logicback.utils.BackTypeEnum;
import com.tdengine.logicback.utils.CmdEnum;
import com.tdengine.logicback.exception.ReadConfigException;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.Statement;

@Component
@Slf4j
@Getter
@Setter
public class MainConfigModel implements Cloneable  {
    private DataBaseConfigModel dataBaseConfigModel;
    //备份文件的类型，是否备份为多个文件 可以将备份数据按照一个表（或者多个表）一个文件（或者多个文件）的形式进行备份
    @Autowired
    private AdvanceOptionModel advanceOptionModel;

    @Autowired
    private MultTaskConfigModel multTaskConfigModel;

    //查询表和超级表
    private DBQueryResModel dbQueryResModel;
    @SneakyThrows
    @Override
    public MainConfigModel clone()
    {
        return (MainConfigModel) super.clone();
    }

}
