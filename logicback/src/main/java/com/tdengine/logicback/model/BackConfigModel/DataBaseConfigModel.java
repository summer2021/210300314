package com.tdengine.logicback.model.BackConfigModel;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

@Getter
@Setter
public class DataBaseConfigModel {
    //数据库名
    private String name;
    //超级表集合
    private HashSet<String> stableHashSet;

    private HashMap<String, HashSet<String>> tableHashMap;

    public DataBaseConfigModel() {
        this.stableHashSet = new HashSet<>();
        this.tableHashMap = new HashMap<>();
    }
}
