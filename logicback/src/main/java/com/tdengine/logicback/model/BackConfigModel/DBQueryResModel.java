package com.tdengine.logicback.model.BackConfigModel;

import java.util.HashMap;
import java.util.HashSet;

public class DBQueryResModel {
    private HashMap<String,String> tableHashMap;
    private HashSet<String> stableHashSet;

    public HashMap<String, String> getTableHashMap() {
        return tableHashMap;
    }

    public void setTableHashMap(HashMap<String, String> tableHashMap) {
        this.tableHashMap = tableHashMap;
    }

    public HashSet<String> getStableHashSet() {
        return stableHashSet;
    }

    public void setStableHashSet(HashSet<String> stableHashSet) {
        this.stableHashSet = stableHashSet;
    }

    public DBQueryResModel() {
        this.stableHashSet=new HashSet<>();
        this.tableHashMap=new HashMap<>();
    }
}
