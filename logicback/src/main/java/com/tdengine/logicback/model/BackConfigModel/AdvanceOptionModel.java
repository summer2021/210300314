package com.tdengine.logicback.model.BackConfigModel;
import com.tdengine.logicback.utils.BackFileTypeEnum;
import com.tdengine.logicback.utils.BackTypeEnum;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
public class AdvanceOptionModel {
    //备份文件的类型，是否备份为多个文件 可以将备份数据按照一个表（或者多个表）一个文件（或者多个文件）的形式进行备份
    @Value("${advanceOption.backFileType}")
    private BackFileTypeEnum backFileType;
    //备份的类型 备份 结构 数据
    @Value("${advanceOption.backType}")
    private BackTypeEnum backTypeEnum;
    @Value("${advanceOption.saveDir}")
    private String saveDir;
}
