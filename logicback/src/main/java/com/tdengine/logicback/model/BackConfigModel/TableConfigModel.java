package com.tdengine.logicback.model.BackConfigModel;

import java.util.HashSet;

public class TableConfigModel {
    //表的姓名
    private String name;
    //如果填了各个字段
    private HashSet<String> columnNameList;

    public TableConfigModel(String name,HashSet<String> columnNameList ) {
        this.columnNameList = columnNameList;
        this.name = name;
    }

    public HashSet<String> getColumnNameList() {
        return columnNameList;
    }

    public void setColumnNameList(HashSet<String> columnNameList) {
        this.columnNameList = columnNameList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
