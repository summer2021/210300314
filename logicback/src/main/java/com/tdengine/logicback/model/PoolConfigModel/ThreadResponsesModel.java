package com.tdengine.logicback.model.PoolConfigModel;

import com.tdengine.logicback.utils.TheradResponsesCodeEnum;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ThreadResponsesModel<T> {

    private TheradResponsesCodeEnum code;
    private String msg;
    private T data;
    private T res;

    public ThreadResponsesModel() {
    }

    public ThreadResponsesModel(TheradResponsesCodeEnum code, String msg, T data, T res) {
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.res = res;
    }
}
