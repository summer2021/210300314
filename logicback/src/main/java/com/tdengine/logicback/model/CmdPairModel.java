package com.tdengine.logicback.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CmdPairModel {
    String cmd;
    String value;
}
