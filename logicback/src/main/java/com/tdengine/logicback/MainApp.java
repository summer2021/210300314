package com.tdengine.logicback;

import com.tdengine.logicback.datasource.DataSourceConnectionFactory;
import com.tdengine.logicback.model.BackConfigModel.MainConfigModel;
import com.tdengine.logicback.model.DataSourceModel;
import com.tdengine.logicback.service.LogicBackMainService;
import com.tdengine.logicback.service.ReadConfigService;
import com.tdengine.logicback.utils.JudgeStrType;
import com.tdengine.logicback.exception.ReadConfigException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
@Slf4j
public class MainApp {
    @Autowired
    private ReadConfigService readConfigService;
    @Autowired
    LogicBackMainService logicBackMainService;
    @Autowired
    private MainConfigModel mainConfigModel;
    @Autowired
    private DataSourceModel dataSourceModel;
    public void readConfig() {
        //初始化datasourceModel
        DataSourceConnectionFactory.dataSourceModel = dataSourceModel;
        System.out.println("请输入命令（详情请输入-h）");
        Scanner scanner = new Scanner(System.in);
        boolean quitFlag=false;
        System.out.print("logicback> ");
        while (scanner.hasNextLine()) {
            MainConfigModel mainConfigModelClone=mainConfigModel.clone();
            try {
                String str = scanner.nextLine();
                log.info(str);
                if(str.trim().length()!=0)
                {
                    readConfigService.readString(mainConfigModel, str);
                }
            } catch (ReadConfigException readConfigException) {
                log.error(readConfigException.getCmdEnum()+" "+readConfigException.getErrMes()+"  "+readConfigException.getMsgDes());
                mainConfigModel=mainConfigModelClone;
                switch (readConfigException.getCmdEnum()) {
                    case QUIT:
                        quitFlag=true;
                        break;
                    case START:
                        quitFlag=true;
                        logicBackMainService.logicBackMain(mainConfigModel);
                        break;
                    case URL:
                        System.out.println(readConfigException.getErrMes() + " " + readConfigException.getMsgDes());
                        break;
                    default:
                        System.out.println(readConfigException.getErrMes() + " " + readConfigException.getMsgDes());
                        break;
                }
            }
            if(quitFlag)
            {
                break;
            }
            System.out.print("logicback> ");
        }
        System.out.println("已结束");
    }
}
