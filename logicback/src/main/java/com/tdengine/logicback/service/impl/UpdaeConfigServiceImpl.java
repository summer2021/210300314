package com.tdengine.logicback.service.impl;

import com.tdengine.logicback.datasource.DataSourceConnectionFactory;
import com.tdengine.logicback.exception.ReadConfigException;
import com.tdengine.logicback.model.BackConfigModel.DataBaseConfigModel;
import com.tdengine.logicback.model.BackConfigModel.MainConfigModel;
import com.tdengine.logicback.service.IsRightConfigService;
import com.tdengine.logicback.service.ReadConfigService;
import com.tdengine.logicback.service.ShowTDengineService;
import com.tdengine.logicback.service.UpdateConfigService;
import com.tdengine.logicback.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.util.*;

@Service
public class UpdaeConfigServiceImpl implements UpdateConfigService {
    @Autowired
    private ReadConfigService readConfigService;
    @Autowired
    private IsRightConfigService isRightConfigService;
    @Autowired
    private ShowTDengineService showTDengineService;

    StringUtil stringUtil = new StringUtil();

    @Override
    public void updateUrl(String value) {
        DataSourceConnectionFactory.dataSourceModel.setUrl(value);
        DataSourceConnectionFactory.updateFlag = true;
    }

    @Override
    public void updateUserName(String value) {
        DataSourceConnectionFactory.dataSourceModel.setUsername(value);
        DataSourceConnectionFactory.updateFlag = true;
    }

    @Override
    public void updatePassword(String value) {
        DataSourceConnectionFactory.dataSourceModel.setPassword(value);
        DataSourceConnectionFactory.updateFlag = true;
    }

    @Override
    public void updateSaveDir(MainConfigModel mainConfigModel, String value) {
        JudgeStrType judgeStrType = new JudgeStrType();
        if (!judgeStrType.haveDir(value)) {
            throw new ReadConfigException(
                    CmdEnum.SAVEDIR, value, "保存的文件夹名错误"
            );
        } else {
            mainConfigModel.getAdvanceOptionModel().setSaveDir(value);
        }
    }

    @Override
    public void updatebackFileType(MainConfigModel mainConfigModel, String value) {
        BackFileTypeEnum backFileTypeEnum = readConfigService.getbackFileTypeEnum(value);
        if (backFileTypeEnum == BackFileTypeEnum.ERRORTYPE) {
            throw new ReadConfigException(
                    CmdEnum.BACKFILETYPE, value, "备份文件类型选择错误"
            );
        } else {
            mainConfigModel.getAdvanceOptionModel().setBackFileType(backFileTypeEnum);
        }
    }

    @Override
    public void updatebackType(MainConfigModel mainConfigModel, String value) {
        BackTypeEnum backTypeEnum=readConfigService.getbackTypeEnum(value);
        if(backTypeEnum==BackTypeEnum.ERRORTYPE)
        {
            throw new ReadConfigException(
                    CmdEnum.BACKTYPE, value, "备份类型选择错误"
            );
        }
        mainConfigModel.getAdvanceOptionModel().setBackTypeEnum(backTypeEnum);
    }

    @Override
    public void updateDataBase(MainConfigModel mainConfigModel, String value) {
        if(value==null||value.length()==0)
        {
            throw new ReadConfigException(CmdEnum.DATABAS,value,"没有输入值");
        }
        Connection connection = DataSourceConnectionFactory.getConnection();
        if (isRightConfigService.isRightDataBase(connection, value)) {
            DataBaseConfigModel dataBaseConfigModel = new DataBaseConfigModel();
            dataBaseConfigModel.setName(value);
            mainConfigModel.setDataBaseConfigModel(dataBaseConfigModel);
        } else {
            throw new ReadConfigException(
                    CmdEnum.DATABAS, value, "没有该数据库"
            );
        }
    }


    @Override
    public void updateTable(MainConfigModel mainConfigModel, String value) {
        if (value.length() == 0) {
            throw new ReadConfigException(CmdEnum.TABLE, value, "没有输入值");
        }
        if (mainConfigModel.getDataBaseConfigModel() == null || mainConfigModel.getDataBaseConfigModel().getName().length() <= 0) {
            throw new ReadConfigException(CmdEnum.DATABAS, "-t", "没有输入数据库名");
        }
        // 首先判断 其格式是否正确 正确的格式 t1(c1,c2),t2,t3,[t4,t5,t6](c1,c2,c3)
        List<String> list = stringUtil.splitTable(value);
        //普通的表 即使没有选择可选列 和 【c1,c2】(0,1)
        HashSet<String> nomalTable = new HashSet<>();
        for (String s : list) {
            //如果s的格式是这样 [t4,t5-t7,t6](c1,c2,c3) 或者 t1-t2(0,1,2),t1(3,4)
            if (s.charAt(s.length() - 1) == ')') {

                updateTablesConfigModel(mainConfigModel, s);
                continue;
            }
            if (s.contains("[")) {
                throw new ReadConfigException(CmdEnum.TABLE, s, "没有选择列");
            }
            //如果s的格式 是 t1-t2
            if (s.contains("-")) {
                nomalTable.addAll(stringUtil.splitTableRange(s));
                continue;
            }
            //如果s的格式是 t1,t2
            nomalTable.add(s);
        }
        if (nomalTable.size() > 0) {
            //下面开始判断普通的表的是否是正确的
            isRightConfigService.isRightTable(mainConfigModel, nomalTable);
            for (String name : nomalTable) {
                mainConfigModel.getDataBaseConfigModel().getTableHashMap().put(name,null);
            }
        }
    }

    @Override
    public void updateTablesConfigModel(MainConfigModel mainConfigModel, String value) {
        HashMap<String,HashSet<String>> tableHashMap = mainConfigModel.getDataBaseConfigModel().getTableHashMap();
        HashSet<String> tables = new HashSet<>();
        HashSet<String> columns = new HashSet<>();
        int i = value.indexOf(']');
        //如果格式是[c1,c2](1,2)
        if (i != -1) {
            String tablesString = value.substring(1, i);
            String columnsString = value.substring(i + 2, value.length() - 1);
            String[] tablesArray = tablesString.split(",");
            stringUtil.removeListSpace(tablesArray);
            for (String s : tablesArray) {
                if (s.contains("-")) {
                    tables.addAll(stringUtil.splitTableRange(s));
                    continue;
                }
                tables.add(s);
            }
            String[] columnsArray = columnsString.split(",");
            stringUtil.removeListSpace(columnsArray);
            columns.addAll(Arrays.asList(columnsArray));
        } else {
            //如果格式是 t1-t2(col1,col2)
            int j = value.indexOf('(');
            String tablesString = value.substring(0, j);
            String columnsString = value.substring(j + 1, value.length() - 1);
            if (tablesString.contains("-")) {
                tables.addAll(stringUtil.splitTableRange(tablesString));
            } else {
                tables.add(tablesString);
            }
            String[] columnsArray = columnsString.split(",");
            stringUtil.removeListSpace(columnsArray);
            columns.addAll(Arrays.asList(columnsArray));
        }
        //首先要判断其是否是正确的表和正确的字段
        isRightConfigService.isRightColumInTables(mainConfigModel,tables,columns);
        for(String name:tables)
        {
            tableHashMap.put(name,columns);
        }
    }

    @Override
    public void updateStable(MainConfigModel mainConfigModel, String value) {
        if(value.length()==0)
        {
            throw new ReadConfigException(CmdEnum.ERRORCMD,value,"没有输入值");
        }
        if (mainConfigModel.getDataBaseConfigModel() == null || mainConfigModel.getDataBaseConfigModel().getName().length() <= 0) {
            throw new ReadConfigException(CmdEnum.DATABAS, "-st", "没有输入数据库名");
        }
        String[] strings = value.split(",");
        stringUtil.removeListSpace(strings);
        HashSet<String> hashSet = new HashSet<>(Arrays.asList(strings));
        isRightConfigService.isRightStable(mainConfigModel.getDbQueryResModel().getStableHashSet(), hashSet);
        mainConfigModel.getDataBaseConfigModel().getStableHashSet().addAll(hashSet);
    }
}
