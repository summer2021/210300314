package com.tdengine.logicback.service.impl;

import com.tdengine.logicback.datasource.DataSourceConnectionFactory;
import com.tdengine.logicback.exception.ReadConfigException;
import com.tdengine.logicback.model.BackConfigModel.MainConfigModel;
import com.tdengine.logicback.model.CmdPairModel;
import com.tdengine.logicback.service.ReadConfigService;
import com.tdengine.logicback.service.ShowTDengineService;
import com.tdengine.logicback.service.UpdateConfigService;
import com.tdengine.logicback.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
@Service
@Slf4j
public class ReadConfigServiceImpl implements ReadConfigService {

    @Autowired
    UpdateConfigService updateConfigService;
    @Autowired
    ShowTDengineService showTDengineService;
    @Autowired
    IsRightConfigServiceImpl isRightConfigService;
    @Override
    public CmdPairModel readCatalog(String str) {
        CmdPairModel cmdPairModel=new CmdPairModel();
        str=str.trim();
        if(str.charAt(0)!='-')
        {
            throw new ReadConfigException(CmdEnum.ERRORCMD,str,"输入错误不可以识别");
        }
        int startIndex=1;
        for(;startIndex<str.length();startIndex++)
        {
            if(str.charAt(startIndex)==' ')
            {
                break;
            }
        }
        cmdPairModel.setCmd(str.substring(1,startIndex).trim());
        if(startIndex<str.length())
        {
            String value=str.substring(startIndex+1).trim();
            cmdPairModel.setValue(value);
        }else {
            cmdPairModel.setValue("");
        }
        return cmdPairModel;
    }

    @Override
    public List<String> splictString(String str) {
        List<String> list=new ArrayList<>();
        str=str.trim();

        if(str.charAt(0)!='-')
        {
            throw new ReadConfigException(CmdEnum.ERRORCMD,str,"字符串识别失败,请输入命令详情请见-h");
        }
        int startIndex=0,endIndex=1;
        for(;startIndex<str.length();startIndex=endIndex-1)
        {
            for(;endIndex<str.length();endIndex++)
            {
                if(str.charAt(endIndex)=='-'&&str.charAt(endIndex-1)==' ')
                {
                    break;
                }
            }
            if(startIndex<endIndex)
            {
                list.add(str.substring(startIndex,endIndex));
            }else {
                break;
            }
            endIndex++;
        }
        return list;
    }

    @Override
    public CmdEnum getCmdEnum(String str) {
        CmdEnum cmdEnum;
        switch (str)
        {
            case "u":cmdEnum=CmdEnum.USERNAME;break;
            case "p":cmdEnum=CmdEnum.PASSWORD;break;
            case "url":cmdEnum= CmdEnum.URL;break;
            case "d":cmdEnum= CmdEnum.DATABAS;break;
            case "t":cmdEnum= CmdEnum.TABLE;break;
            case "st":cmdEnum= CmdEnum.STABLE;break;
            case "sd":cmdEnum= CmdEnum.SAVEDIR;break;
            case "bf":cmdEnum=CmdEnum.BACKFILETYPE;break;
            case "b":cmdEnum=CmdEnum.BACKTYPE;break;
            case "s":cmdEnum=CmdEnum.START;break;
            case "q":cmdEnum= CmdEnum.QUIT;break;
            case "h":cmdEnum=CmdEnum.HELP;break;
            case "test":cmdEnum=CmdEnum.TEST;break;
            default:cmdEnum= CmdEnum.ERRORCMD;break;
        }
        return cmdEnum;
    }

    @Override
    public BackFileTypeEnum getbackFileTypeEnum(String str) {
        BackFileTypeEnum backFileTypeEnum;
        switch (str)
        {
            case "1":backFileTypeEnum=BackFileTypeEnum.ONEFILE;break;
            case "2":backFileTypeEnum=BackFileTypeEnum.MULTFILE;break;
            default:backFileTypeEnum=BackFileTypeEnum.ERRORTYPE;break;
        }
        return backFileTypeEnum;
    }

    @Override
    public BackTypeEnum getbackTypeEnum(String str) {
        BackTypeEnum backTypeEnum;
        switch (str)
        {
            case "1":backTypeEnum=BackTypeEnum.SCHEMA;break;
            case "2":backTypeEnum=BackTypeEnum.DATA;break;
            case "3":backTypeEnum=BackTypeEnum.SCHEMADATA;break;
            default:backTypeEnum=BackTypeEnum.ERRORTYPE;break;
        }
        return backTypeEnum;
    }

    @Override
    public void readCmdpair(MainConfigModel mainConfigModel, CmdPairModel cmdPairModel) {
        CmdEnum cmdEnum=getCmdEnum(cmdPairModel.getCmd());
        switch (cmdEnum)
        {
            case USERNAME:
                updateConfigService.updateUserName(cmdPairModel.getValue());break;
            case PASSWORD:
                updateConfigService.updatePassword(cmdPairModel.getValue());break;
            case URL:
                updateConfigService.updateUrl(cmdPairModel.getValue());break;
            case DATABAS:
                updateConfigService.updateDataBase(mainConfigModel, cmdPairModel.getValue());
                Connection connection = DataSourceConnectionFactory.getConnection();
                mainConfigModel.setDbQueryResModel(showTDengineService.getDataBaseTableInfo(
                        connection, cmdPairModel.getValue()));
                break;
            case TABLE:
                updateConfigService.updateTable(mainConfigModel, cmdPairModel.getValue());
                break;
            case STABLE:
                updateConfigService.updateStable(mainConfigModel, cmdPairModel.getValue());
                break;
            case BACKTYPE:
                updateConfigService.updatebackType(mainConfigModel, cmdPairModel.getValue());
                break;
            case BACKFILETYPE:
                updateConfigService.updatebackFileType(mainConfigModel,cmdPairModel.getValue());
                break;
            case SAVEDIR:
                updateConfigService.updateSaveDir(mainConfigModel,cmdPairModel.getValue());
                break;
            case START:
                throw new ReadConfigException(CmdEnum.START);
            case QUIT:
                throw new ReadConfigException(CmdEnum.QUIT);
            case HELP:
                pringHelp();
                break;
            case TEST:
                isRightConfigService.isRightConnection(DataSourceConnectionFactory.dataSourceModel);
                break;
            default:
                throw new ReadConfigException(
                        cmdEnum,cmdPairModel.getCmd(),"请输入正确的命令（命令详情请查看 -h）"
                );
        }
    }

    @Override
    public void readString(MainConfigModel mainConfigModel, String string) {
        string=string.trim();
        List<String> stringList=splictString(string);
        for(String s:stringList)
        {
            CmdPairModel cmdPairModel=readCatalog(s);
            readCmdpair(mainConfigModel,cmdPairModel);
        }
    }

    @Override
    public void pringHelp() {
        System.out.println("-u username\n" +
                "-p password\n" +
                "-url url\n"+
                "-h help\n" +
                "-test test connection\n" +
                "-bf back type\n"+
                "-bf back file type\n"+
                "-d dataBaseName\n"+
                "-t tableName\n"+
                "-st stableName\n"+
                "-q quit\n"+
                "-s start");
    }
}
