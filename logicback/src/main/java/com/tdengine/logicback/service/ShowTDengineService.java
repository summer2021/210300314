package com.tdengine.logicback.service;
import com.tdengine.logicback.model.BackConfigModel.DBQueryResModel;

import java.sql.Connection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * 这个service 是使用sql语句的的show 和 desc语句
 * @author jeffery
 */

public interface ShowTDengineService {
    /**
     * 查看所有数据库的名字
     * @param connection 数据库的连接
     * @return 返回一个HashSet
     */
    HashSet<String> getDataBaseName(Connection connection);

    /**
     * 查看超级表的名字
     * @param connection 数据库的连接
     * @param dname 数据库名字
     * @return 返回一个HashSet
     */
    HashSet<String> getStableName(Connection connection, String dname);

    /**
     * 查看表的相关信息
     * @param connection 数据库的连接
     * @param dname 数据库名字
     * @return 返回一个HashMap <tableName,stableName>
     */
    HashMap<String,String> getTableMessage(Connection connection, String dname);

    /**
     * 查看表的相关信息
     * @param connection 数据库的连接
     * @param dname 数据库名字
     * @return 返回一个HashSet
     */
    HashSet<String> getTableName(Connection connection, String dname);

    /**
     * 查看单个表的结构信息
     * @param connection 数据库的连接
     * @param dname 数据库名字
     * @param tname  表名字
     * @return 返回结构名的数组
     */
    List<String> getTableColumnName(Connection connection, String dname, String tname);
    /**
     * 查看创建数据可语句
     * @param connection 数据库的连接
     * @param name 数据库名字
     * @return 返回一个sql语句
     */
    String getDataBaseCreateSql(Connection connection,String name);

    /**
     * 查看创建超级表sql语句
     * @param connection 数据库的连接
     * @param dname 数据库名字
     * @param sname  超级表名字
     * @return 返回一个sql语句
     */
    String getStableCreateSql(Connection connection, String dname, String sname);

    /**
     * 查看创建表sql语句
     * @param connection 数据库的连接
     * @param dname 数据库名字
     * @param tname  表名字
     * @return 返回一个sql语句
     */
    String getTableCreateSql(Connection connection, String dname, String tname);

    /**
     *
     * 查看多个创建超级表sql语句
     * @param connection 数据库的连接
     * @param dname 数据库名字
     * @param snameSet  超级表名字
     * @return 返回一组sql语句
     */
    List<String> getStablesCreateSql(Connection connection, String dname, HashSet<String> snameSet);

    /**
     *
     * 查看多个创建超级表sql语句
     * @param connection 数据库的连接
     * @param dname 数据库名字
     * @param tnameList  表名字
     * @return 返回一组sql语句
     */
    List<String> getTablesCreateSqlList(Connection connection, String dname, List<String> tnameList);
    /**
     * 查询该数据库的所有表和超级表
     * @param connection 连接
     * @param dname 数据名称
     * @return DBQueryResModel
     */
    DBQueryResModel getDataBaseTableInfo(Connection connection, String dname);
}
