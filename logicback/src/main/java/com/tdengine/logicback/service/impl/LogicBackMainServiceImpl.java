package com.tdengine.logicback.service.impl;

import com.alibaba.druid.pool.DruidDataSource;
import com.tdengine.logicback.datasource.DataSourceConnectionFactory;
import com.tdengine.logicback.model.BackConfigModel.DataBaseConfigModel;
import com.tdengine.logicback.model.BackConfigModel.MainConfigModel;
import com.tdengine.logicback.model.BackConfigModel.TableConfigModel;
import com.tdengine.logicback.model.PoolConfigModel.BspMultTheradPool;
import com.tdengine.logicback.model.PoolConfigModel.ThreadPoolConfig;
import com.tdengine.logicback.model.PoolConfigModel.ThreadResponsesModel;
import com.tdengine.logicback.service.LogicBackMainService;
import com.tdengine.logicback.service.ShowTDengineService;
import com.tdengine.logicback.task.GetTableDataTask;
import com.tdengine.logicback.task.GetTablesCreateSqlListTask;
import com.tdengine.logicback.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Future;

@Service
@Slf4j
public class LogicBackMainServiceImpl implements LogicBackMainService {
    @Autowired
    private ShowTDengineService showTDengineService;

    private final StringUtil stringUtil =new StringUtil();
    private final MapUtil mapUtil = new MapUtil();
    @Autowired
    private ThreadPoolConfig threadPoolConfig;
    @Override
    public void logicBackMain(MainConfigModel mainConfigModel) {
        try {
            BackFileTypeEnum backFileTypeEnum = mainConfigModel.getAdvanceOptionModel().getBackFileType();
            BackTypeEnum backTypeEnum = mainConfigModel.getAdvanceOptionModel().getBackTypeEnum();
            //开始进行逻辑备份
            System.out.println("开始进行逻辑备份");
            switch (backFileTypeEnum) {
                case ONEFILE:
                    switch (backTypeEnum) {
                        case DATA:
                            backDataOneFile(mainConfigModel);
                            break;
                        case SCHEMA:
                            backSchemaOneFile(mainConfigModel);
                            break;
                        case SCHEMADATA:
                            backSchemaOneFile(mainConfigModel);
                            backDataOneFile(mainConfigModel);
                            break;
                        case ERRORTYPE:
                            break;
                        default:
                            break;
                    }
                    break;
                case MULTFILE:
                    switch (backTypeEnum) {
                        case DATA:
                            backDataMultFile(mainConfigModel);
                            break;
                        case SCHEMA:
                            backSchemaMultFile(mainConfigModel);
                            break;
                        case SCHEMADATA:
                            backSchemaMultFile(mainConfigModel);
                            backDataMultFile(mainConfigModel);
                            break;
                        case ERRORTYPE:
                            break;
                        default:
                            break;
                    }
                    break;
                case ERRORTYPE:
                    break;
                default:
                    break;
            }
            System.out.println("逻辑备份结束");
        }catch (Exception e)
        {
            System.out.println("出现未知错误");
            log.error(e.getMessage());
        }
    }


    @Override
    public void backSchemaOneFile(MainConfigModel mainConfigModel) throws Exception {
        String saveDir = mainConfigModel.getAdvanceOptionModel().getSaveDir();
        String dname = mainConfigModel.getDataBaseConfigModel().getName();
        FileWriteUtil fileWriteUtil = new FileWriteUtil(saveDir,  dname+ ".sql", true);
        backSchemaFile(mainConfigModel,fileWriteUtil);
    }

    @Override
    public void backSchemaMultFile(MainConfigModel mainConfigModel) throws Exception {
        String saveDir = mainConfigModel.getAdvanceOptionModel().getSaveDir();
        FileWriteUtil fileWriteUtil = new FileWriteUtil(saveDir, "schema.sql", true);
        backSchemaFile(mainConfigModel,fileWriteUtil);
    }

    @Override
    public void backDataOneFile(MainConfigModel mainConfigModel) throws Exception {
        DataSource dataSource = DataSourceConnectionFactory.dataSource;
        String dname = mainConfigModel.getDataBaseConfigModel().getName();
        String saveDir = mainConfigModel.getAdvanceOptionModel().getSaveDir();
        List<TableConfigModel> tableConfigModelList = mapUtil.changHashMapToTableConfig
                (mainConfigModel.getDataBaseConfigModel().getTableHashMap());
        System.out.println("开始备份时序");
        FileWriteUtil fileWriteUtil = new FileWriteUtil(saveDir,
                mainConfigModel.getDataBaseConfigModel().getName() + ".sql", false);
        BspMultTheradPool bspMultTheradTaskPool = new BspMultTheradPool(threadPoolConfig);
        //一个超步的运行任务是maxpoolsize的两倍数量
        int group = bspMultTheradTaskPool.getMaximumPoolSize() * 2;
        for (int i = 0; i < tableConfigModelList.size(); ) {
            List<Future> list = new ArrayList<>();
            for (int j = i; j < i + group && j < tableConfigModelList.size(); j++) {
                list.add(bspMultTheradTaskPool.submit(new GetTableDataTask(dataSource, fileWriteUtil
                        , dname, tableConfigModelList.get(j),mainConfigModel.getMultTaskConfigModel())));
            }
            i = i + group;
            //如果i是大于他
            if (i > tableConfigModelList.size()) {
                i = tableConfigModelList.size();
            }
            int errorPoint = 0;
            for (Future future : list) {
                ThreadResponsesModel threadResponsesModel = (ThreadResponsesModel) future.get();
                if (threadResponsesModel.getCode() == TheradResponsesCodeEnum.ERROR) {
                    errorPoint++;
                    TableConfigModel tempData = (TableConfigModel) threadResponsesModel.getData();
                    tableConfigModelList.add(tempData);
                }
            }
            bspMultTheradTaskPool.updateCoreAndMaxPoolSize(errorPoint);
            group = bspMultTheradTaskPool.getMaximumPoolSize() * 2;
            System.out.println("时序数据已完成了: "+stringUtil.finshCompareRate(i,tableConfigModelList.size()));
        }
        fileWriteUtil.close();
        System.out.println("时序数据查询结束");
        bspMultTheradTaskPool.close();
    }
    @Override
    public void backDataMultFile(MainConfigModel mainConfigModel) throws Exception {
        DataSource dataSource = DataSourceConnectionFactory.dataSource;
        String dname = mainConfigModel.getDataBaseConfigModel().getName();
        String saveDir = mainConfigModel.getAdvanceOptionModel().getSaveDir();
        List<TableConfigModel> tableConfigModelList = mapUtil.changHashMapToTableConfig
                (mainConfigModel.getDataBaseConfigModel().getTableHashMap());
        System.out.println("开始备份时序");
        BspMultTheradPool bspMultTheradTaskPool = new BspMultTheradPool(threadPoolConfig);
        //一个超步的运行任务是maxpoolsize的两倍数量
        int group = bspMultTheradTaskPool.getMaximumPoolSize() * 2;
        for (int i = 0; i < tableConfigModelList.size(); ) {
            List<Future> list = new ArrayList<>();
            for (int j = i; j < i + group && j < tableConfigModelList.size(); j++) {
                TableConfigModel tableConfigModel = tableConfigModelList.get(j);
                FileWriteUtil fileWriteUtil = new FileWriteUtil(saveDir,
                        tableConfigModel.getName() + ".sql", true);
                list.add(bspMultTheradTaskPool.submit(new GetTableDataTask(dataSource, fileWriteUtil
                        , dname, tableConfigModel,mainConfigModel.getMultTaskConfigModel())));

            }
            i = i + group;
            //如果i是大于他
            if (i > tableConfigModelList.size()) {
                i = tableConfigModelList.size();
            }
            int errorPoint = 0;
            for (Future future : list) {
                ThreadResponsesModel threadResponsesModel = (ThreadResponsesModel) future.get();
                if (threadResponsesModel.getCode() == TheradResponsesCodeEnum.ERROR) {
                    errorPoint++;
                    TableConfigModel tempData = (TableConfigModel) threadResponsesModel.getData();
                    tableConfigModelList.add(tempData);
                }
            }
            bspMultTheradTaskPool.updateCoreAndMaxPoolSize(errorPoint);
            group = bspMultTheradTaskPool.getMaximumPoolSize() * 2;

            System.out.println("时序数据已完成了: "+stringUtil.finshCompareRate(i,tableConfigModelList.size()));
        }
        System.out.println("时序数据查询结束");
        bspMultTheradTaskPool.close();
    }
    @Override
    public void backSchemaFile(MainConfigModel mainConfigModel,FileWriteUtil fileWriteUtil) throws Exception {
        System.out.println("开始查询结构数据");
        // 查询 数据库和超级表
        String dname = mainConfigModel.getDataBaseConfigModel().getName();
        DataBaseConfigModel dataBaseConfigModel = mainConfigModel.getDataBaseConfigModel();
        Set<String> tableSet = dataBaseConfigModel.getTableHashMap().keySet();
        DruidDataSource dataSource = DataSourceConnectionFactory.dataSource;
        Connection connection = dataSource.getConnection();
        String dataBaseCreateSql = showTDengineService.getDataBaseCreateSql(connection, mainConfigModel.getDataBaseConfigModel().getName());
        List<String> stablesCreateSqlList = showTDengineService.getStablesCreateSql(connection, dataBaseConfigModel.getName(), dataBaseConfigModel.getStableHashSet());
        connection.close();
        //写入数据
        fileWriteUtil.writeFileString("#创建数据库", dataBaseCreateSql);
        fileWriteUtil.writeFileList("#创建超级表", stablesCreateSqlList);
        //查询普通表
        List<String> tableList = new ArrayList<>(tableSet);
        int sqlAmount = mainConfigModel.getMultTaskConfigModel().getSchemaTaskSqlMount();
        List<List<String>> taskDataList = mapUtil.mapListToList(tableList, sqlAmount);
        BspMultTheradPool bspMultTheradTaskPool = new BspMultTheradPool(threadPoolConfig);
        //一个超步的运行任务是maxpoolsize的两倍数量
        int group = bspMultTheradTaskPool.getMaximumPoolSize() * 2;
        for (int i = 0; i < taskDataList.size(); ) {
            List<Future> list = new ArrayList<>();
            for (int j = i; j < i + group && j < taskDataList.size(); j++) {
                list.add(bspMultTheradTaskPool.submit(new GetTablesCreateSqlListTask(
                        dataSource, dname, taskDataList.get(j)
                )));
            }
            i = i + group;
            //如果i是大于他
            if (i > taskDataList.size()) {
                i = taskDataList.size();
            }
            int errorPoint = 0;
            for (Future future : list) {
                ThreadResponsesModel threadResponsesModel = (ThreadResponsesModel) future.get();
                if (threadResponsesModel.getCode() == TheradResponsesCodeEnum.SUCCESS) {
                    List<String> tempRes = (List<String>) threadResponsesModel.getRes();
                    fileWriteUtil.writeFileList("#写入表", tempRes);
                } else {
                    errorPoint++;
                    List<String> tempData = (List<String>) threadResponsesModel.getData();
                    taskDataList.add(tempData);
                }
            }
            bspMultTheradTaskPool.updateCoreAndMaxPoolSize(errorPoint);
            group = bspMultTheradTaskPool.getMaximumPoolSize() * 2;
            System.out.println("结构数据已完成了: "+stringUtil.finshCompareRate(i,taskDataList.size()));
        }
        fileWriteUtil.close();
        System.out.println("结构数数据查询完毕");
        bspMultTheradTaskPool.close();
    }
}

