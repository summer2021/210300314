package com.tdengine.logicback.service;

import com.tdengine.logicback.model.BackConfigModel.MainConfigModel;
import com.tdengine.logicback.model.DataSourceModel;

import java.sql.Connection;
import java.util.HashSet;

public interface IsRightConfigService {
    /**
     * 测试数据库的连接是否正确
     */
    void isRightConnection(DataSourceModel dataSourceModel);
    /**
     * 判断数据源是否是正确的
     * @param connection 数据库连接
     * @param name 数据库姓名
     * @return 如果可以连接到数据库则返回真 否则返回假
     */
    boolean isRightDataBase(Connection connection,String name);
    /**
     * 判断超级表是否是正确的
     * @param stableHashSet 超级表的名字
     * @param hashSet 要查询超级表的名字
     */
    void isRightStable(HashSet<String> stableHashSet, HashSet<String> hashSet);
    /**
     * 判断表是否是正确的 这里同时查询表的超级表
     * @param mainConfigModel
     * @param  tableList 要查询表的名字
     */
    void isRightTable(MainConfigModel mainConfigModel, HashSet<String> tableList);
    /**
     * 判断表的字段是否是正确的
     * @param connection 数据源的连接
     * @param dname 数据库的名字
     * @param name 表的名字
     */
    void isRightColum(Connection connection, String dname, String name, HashSet<String> column);
    /**
     * 判断表的字段是否是正确的 同时看看自己的超级表
     * @param mainConfigModel 主配置
     * @param tableList 表的list的名字
     */
    void isRightColumInTables(MainConfigModel mainConfigModel, HashSet<String> tableList, HashSet<String> column);

}
