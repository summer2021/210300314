package com.tdengine.logicback.service;

import com.tdengine.logicback.model.BackConfigModel.MainConfigModel;

public interface UpdateConfigService {

    void updateUrl(String value);
    void updateUserName(String value);
    void updatePassword(String value);
    /**
     * 更新保存的路径
     * @param mainConfigModel
     * @param value 值
     */
    void updateSaveDir(MainConfigModel mainConfigModel,String value);

    /**
     * 更新备份文件的类型可以多个文件类型
     * @param mainConfigModel 配置
     * @param value 值
     */
    void updatebackFileType(MainConfigModel mainConfigModel,String value);
    /**
     * 更新备份文件的类型可以多个文件类型
     * @param mainConfigModel 配置
     * @param value 值
     */
    void updatebackType(MainConfigModel mainConfigModel,String value);
    /**
     * 更新数据库
     * @param mainConfigModel 主配置
     * @param value 值
     */
    void updateDataBase(MainConfigModel mainConfigModel,String value);


    /**
     * 更新表
     * @param mainConfigModel 主配置
     * @param value 值
     */
    void updateTable(MainConfigModel mainConfigModel,String value);

    /**
     * 更新一个字段的值
     * @param mainConfigModel 保存的表
     * @param value 值
     */
    void updateTablesConfigModel(MainConfigModel mainConfigModel, String value);

    /**
     * 更新超级表
     * @param mainConfigModel 主配置
     * @param value 值
     */
    void updateStable(MainConfigModel mainConfigModel,String value);
}
