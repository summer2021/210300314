package com.tdengine.logicback.service;

import com.tdengine.logicback.model.BackConfigModel.MainConfigModel;
import com.tdengine.logicback.model.CmdPairModel;
import com.tdengine.logicback.utils.BackFileTypeEnum;
import com.tdengine.logicback.utils.BackTypeEnum;
import com.tdengine.logicback.utils.CmdEnum;

import java.util.List;

public interface ReadConfigService {
     /**
      * 从一个命令对中 提取命令和值
      * @param str 命令对是字符串
      * @return 返会 值和命令
      */
    CmdPairModel readCatalog(String str);

    /**
     * 把字符串分成多个命令对
     * @param str 字符串
     * @return 返回 命令对String
     */
    List<String> splictString(String str);

    /**
     *根据字符串得到命令枚举
     * @param str 命令
     * @return 枚举类型
     */
    CmdEnum getCmdEnum(String str);

    /**
     *根据字符串得到备份类型的枚举
     * @param str 字符串
     * @return 枚举类型
     */
    BackFileTypeEnum getbackFileTypeEnum(String str);
    /**
     *根据字符串得到备份类型的枚举
     * @param str 字符串
     * @return 枚举类型
     */
    BackTypeEnum getbackTypeEnum(String str);
    /**
     * 读取命令
     * @param cmdPairModel 包含命令 值
     * @param mainConfigModel 返回的列
     */
    void readCmdpair(MainConfigModel mainConfigModel,CmdPairModel cmdPairModel);

    /**
     * 从字符串中读取信息
     * @param mainConfigModel 读取后存储的地方
     * @param string 字符串
     */
    void readString(MainConfigModel mainConfigModel,String string);

    /**
     * 打印菜单
     */
    void pringHelp();

}
