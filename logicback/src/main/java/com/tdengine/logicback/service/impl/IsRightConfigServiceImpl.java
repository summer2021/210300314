package com.tdengine.logicback.service.impl;

import com.alibaba.druid.pool.DruidDataSource;
import com.tdengine.logicback.datasource.DataSourceConnectionFactory;
import com.tdengine.logicback.model.BackConfigModel.MainConfigModel;
import com.tdengine.logicback.model.DataSourceModel;
import com.tdengine.logicback.service.IsRightConfigService;
import com.tdengine.logicback.service.ShowTDengineService;
import com.tdengine.logicback.utils.CmdEnum;
import com.tdengine.logicback.exception.ReadConfigException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

@Service
@Slf4j
public class IsRightConfigServiceImpl implements IsRightConfigService {
    @Autowired
    private ShowTDengineService showTDengineService;

    @Override
    public void isRightConnection(DataSourceModel dataSourceModel) {
        try {
            DruidDataSource dataSource= DataSourceConnectionFactory.getDataSource(dataSourceModel); // get connection
            //测试连接时我们只重复连接3次
            dataSource.setConnectionErrorRetryAttempts(3);
            Connection connection=dataSource.getConnection();
            connection.close();
            dataSource.close();
        } catch (Exception e)
        {
            log.error(e.getMessage());
            throw new ReadConfigException(CmdEnum.URL,"","数据库连接错误,请检查用户名,密码，和JDBC URL");
        }
    }

    @Override
    public boolean isRightDataBase(Connection connection, String name) {
        HashSet<String> hashSet=showTDengineService.getDataBaseName(connection);
        return hashSet.contains(name);
    }

    @Override
    public void isRightStable(HashSet<String> stableHashSet, HashSet<String> hashSet) {
        for(String s:hashSet)
        {
            if(!stableHashSet.contains(s))
            {
               throw new ReadConfigException(CmdEnum.STABLE,s,"没有该超级表");
            }
        }
    }

    @Override
    public void isRightTable(MainConfigModel mainConfigModel, HashSet<String> tableList) {
        HashMap<String,String> tableHashMap=mainConfigModel.getDbQueryResModel().getTableHashMap();
        for(String s: tableList)
        {
            if(!tableHashMap.containsKey(s))
            {
                throw new ReadConfigException(CmdEnum.TABLE,s,"没有该表");
            }
        }
    }

    @Override
    public void isRightColum(Connection connection, String dname, String name, HashSet<String> column) {
        List<String> tableColumnName=showTDengineService.getTableColumnName(connection,dname,name);
        String firstName=tableColumnName.get(0);
        if(!column.contains(firstName))
        {
            throw new ReadConfigException(CmdEnum.TABLE,firstName,"必须包含该字段");
        }
        for(String s:column)
        {
            if(!tableColumnName.contains(s))
            {
                throw new ReadConfigException(CmdEnum.TABLE,s,"没有该字段");
            }
        }
    }

    @Override
    public void isRightColumInTables(MainConfigModel mainConfigModel, HashSet<String> tableList, HashSet<String> column) {
        List<String> table=new ArrayList<>();
        HashSet<String> stable=new HashSet<>();
        HashMap<String,String> tableHashMap=mainConfigModel.getDbQueryResModel().getTableHashMap();
        for(String s:tableList)
        {

            if(!tableHashMap.containsKey(s))
            {
                throw new ReadConfigException(CmdEnum.TABLE,s,"没有该表");
            }
            //拥有相同超级表的子表，只需要查看一次子表是否包含这些字段
            if(!stable.contains(tableHashMap.get(s)))
            {
                if(tableHashMap.get(s).length()!=0)
                {
                    table.add(s);
                }
                stable.add(tableHashMap.get(s));
            }
        }
        for(String s:table)
        {
            Connection connection = DataSourceConnectionFactory.getConnection();
            isRightColum(connection,mainConfigModel.getDataBaseConfigModel().getName(),s,column);
            DataSourceConnectionFactory.closeConnection(connection);
        }
    }
}
