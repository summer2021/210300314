package com.tdengine.logicback.service;

import com.tdengine.logicback.model.BackConfigModel.MainConfigModel;
import com.tdengine.logicback.model.BackConfigModel.TableConfigModel;
import com.tdengine.logicback.utils.FileWriteUtil;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ExecutionException;

public interface LogicBackMainService {
    void logicBackMain(MainConfigModel mainConfigModel);
    void backSchemaOneFile(MainConfigModel mainConfigModel) throws Exception;
    void backSchemaMultFile(MainConfigModel mainConfigModel) throws Exception;
    void backDataOneFile(MainConfigModel mainConfigModel) throws Exception;
    void backDataMultFile(MainConfigModel mainConfigModel) throws Exception;
    void backSchemaFile(MainConfigModel mainConfigModel,FileWriteUtil fileWriteUtil) throws Exception;
}
