package com.tdengine.logicback;

import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author jeffery
 */
@SpringBootConfiguration
@ComponentScan
public class LogicBackSpringBootApp implements CommandLineRunner {
    public static void main(String[] args) throws Exception {
        SpringApplication application= new SpringApplication(LogicBackSpringBootApp.class);
        application.setBannerMode(Banner.Mode.OFF);
        application.run(args);
    }

    @Autowired
    private MainApp mainApp;

    @Override
    public void run(String... args) throws Exception {
        mainApp.readConfig();
        //mainApp.testConfig();
    }
}
