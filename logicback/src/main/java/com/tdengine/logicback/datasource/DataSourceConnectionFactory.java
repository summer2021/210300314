package com.tdengine.logicback.datasource;

import com.alibaba.druid.pool.DruidDataSource;
import com.tdengine.logicback.model.DataSourceModel;
import com.tdengine.logicback.utils.CmdEnum;
import com.tdengine.logicback.exception.ReadConfigException;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

//使用这个简单工厂类创建连接
@Slf4j
public  class DataSourceConnectionFactory {
    public static DruidDataSource dataSource = null;

    public static DataSourceModel dataSourceModel = null;

    //标记已经更新
    public static boolean updateFlag = true;

    //创建DruidDataSource
    public static DruidDataSource getDataSource(DataSourceModel dataSourceModel)
    {
        DruidDataSource dataSource = new DruidDataSource();
        // jdbc properties
        dataSource.setDriverClassName(dataSourceModel.getDriverClassName());
        dataSource.setUrl(dataSourceModel.getUrl());
        dataSource.setUsername(dataSourceModel.getUsername());
        dataSource.setPassword(dataSourceModel.getPassword());
        dataSource.setConnectionErrorRetryAttempts(dataSourceModel.getConnectionErrorRetryAmount());
        // pool configurations
        dataSource.setInitialSize(dataSourceModel.getInitialSize());
        dataSource.setMinIdle(dataSourceModel.getMinIdle());
        dataSource.setMaxActive(dataSourceModel.getMaxActive());
        dataSource.setMaxWait(dataSourceModel.getMaxWait());
        dataSource.setValidationQuery(dataSourceModel.getValidationQuery());
        return dataSource;
    }

    public static Connection getConnection()  {
        try {
            if(updateFlag)
            {
                dataSource = getDataSource(dataSourceModel);
                updateFlag = false;
            }
            return dataSource.getConnection();
        }catch (Exception exception)
        {
            log.error(exception.getMessage());
            throw new ReadConfigException(CmdEnum.URL,"","数据库连接错误,请检查用户名,密码，和JDBC URL");
        }
    }
    public static void closeConnection(Connection connection){
        try {
            connection.close();
        } catch (SQLException e) {
            log.error(e.getMessage());
            throw new ReadConfigException(CmdEnum.ERRORCMD,"","数据库连接发生错误");
        }
    }
}
