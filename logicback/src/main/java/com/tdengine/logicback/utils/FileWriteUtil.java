package com.tdengine.logicback.utils;

import com.tdengine.logicback.exception.FileWirteException;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

@Getter
@Setter
@Slf4j
public class FileWriteUtil {
    /**
     * 文件夹名
     */
    private String dir;

    /**
     * 文件名
     */
    private String fileName;
    /**
     * 文件
     */
    private File file;

    /**
     * 是否删除文件
     */
    private Boolean deleteExistFile;
    //输入流
    private BufferedWriter bufferedWriter;

    public FileWriteUtil(String dir, String fileName, Boolean deleteExistFile) {
        this.fileName = fileName;
        this.dir = dir;
        this.deleteExistFile = deleteExistFile;
        justifyExistFile();
        createBufferWritter();
    }

    public void justifyExistFile() {
        if (createExistFile()) {
            file = new File(dir, fileName);
        } else {
            throw new FileWirteException("创建文件失败", "文件和文件夹发生错误");
        }
    }

    @SneakyThrows
    private void createBufferWritter() {
        bufferedWriter = new BufferedWriter(new FileWriter(file,true));
    }

    @SneakyThrows
    public Boolean createExistFile() {
        //2.  创建文件夹对象     创建文件对象
        File file = new File(dir);
        //如果文件夹不存在  就创建一个空的文件夹
        if (!file.exists()) {
            if (!file.mkdirs()) {
                return false;
            }
        }
        File file2 = new File(dir, fileName);
        //如果文件存在  清空群文件 否则创建文件
        if (file2.exists()) {
            if (deleteExistFile) {
                FileWriter fileWriter = new FileWriter(file2);
                fileWriter.write("");
                fileWriter.flush();
                fileWriter.close();
            }
        } else {
            try {
                return file2.createNewFile();
            } catch (IOException e) {
                log.error(e.getMessage());
                return false;
            }
        }
        return true;
    }

    @SneakyThrows
    public void close() {
        bufferedWriter.close();
    }

    @SneakyThrows
    public synchronized void writeFileList(String desc, List<String> dataList) {

        //写入描述文件
        if (desc.length() > 0) {
            bufferedWriter.write("#" + desc);
            bufferedWriter.newLine();
        }
        for (String s : dataList) {
            bufferedWriter.write(s);
            bufferedWriter.newLine();
        }
        bufferedWriter.flush();
    }

    @SneakyThrows
    public synchronized void writeFileString(String desc, String data) {

        //写入描述文件
        if (desc.length() > 0) {
            bufferedWriter.write("#" + desc);
            bufferedWriter.newLine();
        }
        bufferedWriter.write(data);
        bufferedWriter.newLine();
        bufferedWriter.flush();

    }

}
