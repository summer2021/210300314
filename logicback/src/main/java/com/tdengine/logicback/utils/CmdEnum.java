package com.tdengine.logicback.utils;

public enum CmdEnum
{
    //用户名
    USERNAME("u"),
    //密码
    PASSWORD("p"),
    //链接
    URL("url"),
    //数据库
    DATABAS("d"),
    //表
    TABLE("t"),
    //超级表
    STABLE("st"),
    //保存的地址
    SAVEDIR("sd"),
    //备份文件类型
    BACKFILETYPE("bf"),
    //备份类型
    BACKTYPE("b"),
    //开始执行命令
    START("s"),
    //帮助
    HELP("h"),
    //退出
    QUIT("q"),
    //测试连接
    TEST("test"),
    //错误
    ERRORCMD("err");

    private final String cmd;
    CmdEnum(String cmd) {
        this.cmd=cmd;
    }

    public String getCmd() {
        return cmd;
    }
}
