package com.tdengine.logicback.utils;

import com.tdengine.logicback.model.BackConfigModel.TableConfigModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class MapUtil {

   public List<List<String>> mapListToList(List<String> list,int amount)
    {
        List<List<String>> res=new ArrayList<>();
        int i;
        for(i=0;i<list.size()-amount;i+=amount)
        {
            res.add(list.subList(i,i+amount));
        }
        if(i<list.size())
        {
            res.add(list.subList(i,list.size()));
        }
        return res;
    }
    public List<TableConfigModel> changHashMapToTableConfig(HashMap<String, HashSet<String>> tableHashMap)
    {
        List<TableConfigModel> res = new ArrayList<>();
        for(String key : tableHashMap.keySet())
        {
            res.add(new TableConfigModel(key,tableHashMap.get(key)));
        }
        return res;
    }

}
