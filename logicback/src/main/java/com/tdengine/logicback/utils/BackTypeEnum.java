package com.tdengine.logicback.utils;

public enum BackTypeEnum {
    /**
     * 结构
     */
    SCHEMA(1),
    /**
     * 数据
     */
    DATA(2),
    /**
     * 结构和数据
     */
    SCHEMADATA(3),
    ERRORTYPE(999);
    private final int val;
    BackTypeEnum(Integer val) {
        this.val=val;
    }

    public int getVal() {
        return val;
    }
}
