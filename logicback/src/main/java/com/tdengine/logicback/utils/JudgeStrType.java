package com.tdengine.logicback.utils;

import java.io.File;

/**
 * 判断字符串类型
 */
public class JudgeStrType {
    /**
     * 判断是否存在该文件夹
     */
    public boolean haveDir(String str) {
        try {
            File file = new File(str);
            if (file.exists()) {
                if (!file.isDirectory()) {
                   return file.mkdir();
                }
            } else {
                return file.mkdir();
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
