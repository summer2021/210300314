package com.tdengine.logicback.utils;

import com.alibaba.druid.sql.visitor.functions.Char;
import com.tdengine.logicback.exception.ReadConfigException;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
@Slf4j
public class StringUtil {
   public List<String> splitTable(String s)
    {
        int startIndex=0;
        int endIndex=0;
        List<String> list=new ArrayList<>();
        /**
         * 匹配符合[
         */
        boolean bagFlag1=true;
        /**
         * 匹配符号(
         */
        boolean bagFlag2=true;
        for(;endIndex<s.length();endIndex++)
        {
            if(bagFlag1&&bagFlag2)
            {
                if(s.charAt(endIndex)==',')
                {
                    list.add(s.substring(startIndex,endIndex));
                    startIndex=endIndex+1;
                }
            }
            switch (s.charAt(endIndex))
            {
                case '[':bagFlag1=!bagFlag1;break;
                case ']':bagFlag1=!bagFlag1;break;
                case '(':bagFlag2=!bagFlag2;break;
                case ')':bagFlag2=!bagFlag2;break;
                default:break;
            }
        }
        list.add(s.substring(startIndex));
        for (int i=0;i<list.size();i++)
        {
            list.set(i,list.get(i).trim());
        }
        return list;
    }

    public List<String> splitTableRange(String s)
    {
        List<String> list=new ArrayList<>();
        String[] temp=s.split("-");
        if(temp.length!=2)
        {
            throw new ReadConfigException(CmdEnum.TABLE,s,"table的范围选择错误");
        }
        removeListSpace(temp);
        int starti=getIntIndex(temp[0]);
        int endi=getIntIndex(temp[1]);
        String startString=temp[0].substring(0,starti);
        String endString=temp[1].substring(0,endi);
        if(!startString.equals(endString)) {
            throw new ReadConfigException(CmdEnum.TABLE,s,"table的范围选择错误");
        }
        try {
            int start= Integer.parseInt(temp[0].substring(starti));
            int end= Integer.parseInt(temp[1].substring(endi));
            if(start>end) {
                throw new ReadConfigException(CmdEnum.TABLE,s,"table的范围选择错误");
            }
            for(;start<=end;start++)
            {
                list.add(startString+String.valueOf(start));
            }
        }catch (Exception ignored)
        {
            log.error(ignored.getMessage());
            throw new ReadConfigException(CmdEnum.TABLE,s,"table的范围选择错误");
        }
        return list;
    }

    /**
     * 得到字符串后的int的Index
     * @param str
     * @return 不同的index
     */
    public int getIntIndex(String str)
    {
        int i=str.length()-1;
        for(;i>=0;i--)
        {
            char c=str.charAt(i);
            if(c>'9'||c<'0') {
                break;
            }
        }
        i++;
        //删除相同的0
        for(;i<str.length();i++)
        {
            if(str.charAt(i)!=0)
            {
                break;
            }
        }
        return i;
    }

    /**
     * 去除字符串列表的空格
     * @param strings 字符串列表
     */
    public void removeListSpace(String[] strings)
    {
        for(int i=0;i<strings.length;i++)
        {
            strings[i]=strings[i].trim();
        }
    }

    public String collectionToString(Collection collection)
    {
        StringBuilder string= new StringBuilder();
        string.append('(');
        for(Object object:collection)
        {
            string.append('\'').append(String.valueOf(object)).append("',");
        }
        string.deleteCharAt(string.length() - 1);
        return string.append(')').toString();
    }
    public String finshCompareRate(int haveFinish,int allAmount)
    {
        Double  d=  (double) haveFinish / (double) allAmount;
        java.text.NumberFormat percentFormat =java.text.NumberFormat.getPercentInstance();

        percentFormat.setMaximumFractionDigits(2); //最大小数位数

        percentFormat.setMaximumIntegerDigits(3);//最大整数位数

        percentFormat.setMinimumFractionDigits(2); //最小小数位数

        percentFormat.setMinimumIntegerDigits(1);//最小整数位数

        return percentFormat.format(d);//自动转换成百分比显示
    }

}
