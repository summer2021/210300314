package com.example.logicbackfrontback.exception;

public class FileWirteException extends RuntimeException{
    /**
     * 错误信息
     */
    private String errMes ;
    /**
     * 异常对应的描述信息
     */
    private String msgDes;

    public FileWirteException(String errMes, String msgDes) {
        this.errMes = errMes;
        this.msgDes = msgDes;
    }

    public FileWirteException(String message, String errMes, String msgDes) {
        super(message);
        this.errMes = errMes;
        this.msgDes = msgDes;
    }
}
