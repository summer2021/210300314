package com.example.logicbackfrontback.task;

import com.example.logicbackfrontback.model.backConfigModel.MultTaskConfigModel;
import com.example.logicbackfrontback.model.backConfigModel.TableConfigModel;
import com.example.logicbackfrontback.model.poolConfigModel.ThreadResponsesModel;
import com.example.logicbackfrontback.util.FileWriteUtil;
import com.example.logicbackfrontback.util.StringUtil;
import com.example.logicbackfrontback.util.TheradResponsesCodeEnum;
import lombok.extern.slf4j.Slf4j;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

@Slf4j
public class GetTableDataTask implements Callable {
    private final DataSource dataSource;
    private final String dname;
    private int columnCount;
    private boolean noSetInitConfig;
    private String sqlFront;
    private final TableConfigModel tableConfigModel;
    private final StringUtil stringUtil;
    private final FileWriteUtil fileWriteUtil;
    private final MultTaskConfigModel multTaskConfigModel;
    public GetTableDataTask(DataSource dataSource, FileWriteUtil fileWriteUtil, String dname, TableConfigModel tableConfigModel,
                            MultTaskConfigModel multTaskConfigModel) {
        this.dataSource = dataSource;
        this.dname = dname;
        this.fileWriteUtil=fileWriteUtil;
        this.tableConfigModel = tableConfigModel;
        stringUtil=new StringUtil();
        noSetInitConfig=true;
        this.multTaskConfigModel=multTaskConfigModel;
    }

    private List<String> getTableDataAllColumn(Connection connection, int startIndex,int amount) throws SQLException {
        List<String> res = new ArrayList<>();
        // get statement
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("select * from "+tableConfigModel.getName()+" limit "+startIndex+","+amount);
        if(noSetInitConfig)
        {
            noSetInitConfig=false;
            ResultSetMetaData resultSetMetaData=resultSet.getMetaData();
            columnCount=resultSetMetaData.getColumnCount();
            sqlFront="INSERT INTO "+tableConfigModel.getName()+" VALUES ";
        }
        while (resultSet.next())
        {
            List<String> columnValue=new ArrayList<>();
            for(int i=1;i<=columnCount;i++)
            {
             columnValue.add(resultSet.getString(i));
            }
            res.add(sqlFront+stringUtil.collectionToString(columnValue)+";");
        }
        return res;
    }

    private List<String> getTableDataPartColumn(Connection connection, int startIndex,int amount) throws SQLException {
        List<String> res = new ArrayList<>();
        // get statement
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("select * from "+tableConfigModel.getName()+" limit "+startIndex+","+amount);
        if(noSetInitConfig)
        {
            noSetInitConfig=false;
            sqlFront="INSERT INTO "+tableConfigModel.getName()+" " +stringUtil.collectionToString(tableConfigModel.getColumnNameList())+" VALUES " ;
        }
        ResultSetMetaData metaData = resultSet.getMetaData();
        while (resultSet.next())
        {
            List<String> columnValue=new ArrayList<>();
            for(String s:tableConfigModel.getColumnNameList())
            {
                columnValue.add(resultSet.getString(s));
            }
            res.add(sqlFront+stringUtil.collectionToString(columnValue)+";");
        }
        return res;
    }
    public void saveAllColumnTableData(Connection connection) throws  SQLException
    {
        int dataCount=multTaskConfigModel.getQueryDataCount();
        int dataRepeatCount=multTaskConfigModel.getQueryDataRepeatCount();
        int startIndex=0;
        while (true)
        {
            List<String> list=getTableDataAllColumn(connection,startIndex,dataCount);
            /**
             *
             * 开始备份数据
             */
            fileWriteUtil.writeFileList("",list);
            if(list.size()<dataCount)
            {
                break;
            }
            /**
             * 如果在读取的时候同时被写入了数据，我们需要多备份一些数据
             */
            startIndex+=dataCount-dataRepeatCount;
        }
    }
    public void savePartColumnTableData(Connection connection) throws  SQLException
    {
        int dataCount=multTaskConfigModel.getQueryDataCount();
        int dataRepeatCount=multTaskConfigModel.getQueryDataRepeatCount();
        int startIndex=0;
        while (true)
        {
            List<String> list=getTableDataPartColumn(connection,startIndex,dataCount);
            /**
             *
             * 开始备份数据
             */
            fileWriteUtil.writeFileList("",list);
            if(list.size()<dataCount)
            {
                break;
            }
            /**
             * 如果在读取的时候同时被写入了数据，我们需要多备份一些数据
             */
            startIndex+=dataCount-dataRepeatCount;
        }
    }

    @Override
    public ThreadResponsesModel call() {
        ThreadResponsesModel threadResponsesModel;
        try {
            Connection connection = dataSource.getConnection();
            Statement statement = connection.createStatement();
            statement.execute("use "+dname);
            if(tableConfigModel.getColumnNameList()!=null&&tableConfigModel.getColumnNameList().size()>0)
            {
                savePartColumnTableData(connection);
            }else {
                saveAllColumnTableData(connection);
            }
            fileWriteUtil.close();
            threadResponsesModel = new ThreadResponsesModel(TheradResponsesCodeEnum.SUCCESS, "", null,
                   "");
            connection.close();
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            threadResponsesModel = new ThreadResponsesModel(TheradResponsesCodeEnum.ERROR, e.getMessage(), tableConfigModel, null);
        }
        return threadResponsesModel;
    }
}
