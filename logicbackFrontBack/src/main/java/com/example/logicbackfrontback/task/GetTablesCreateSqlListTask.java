package com.example.logicbackfrontback.task;

import com.example.logicbackfrontback.model.poolConfigModel.ThreadResponsesModel;
import com.example.logicbackfrontback.util.TheradResponsesCodeEnum;
import lombok.extern.slf4j.Slf4j;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

@Slf4j
public class GetTablesCreateSqlListTask implements Callable {
    DataSource dataSource;
    String dname;
    List<String> tnameList;

    public GetTablesCreateSqlListTask(DataSource dataSource, String dname, List<String> tnameList) {
        this.dataSource = dataSource;
        this.dname = dname;
        this.tnameList = tnameList;
    }

    private List<String> getTablesCreateSqlList(Connection connection, String dname, List<String> tnameList) throws SQLException {
        List<String> res = new ArrayList<>();
        // get statement
        Statement statement = connection.createStatement();
        statement.execute("use "+dname);
        for (String s : tnameList) {
            ResultSet resultSet = statement.executeQuery("SHOW CREATE TABLE " + s + ";");
            if (resultSet.next()) {
                String temp = resultSet.getString("Create Table");
                res.add(temp.substring(0, 12) + " IF NOT EXISTS" + temp.substring(12));
            }
        }
        return res;
    }

    @Override
    public ThreadResponsesModel call() {
        ThreadResponsesModel threadResponsesModel;
        try {
            Connection connection = dataSource.getConnection();
            threadResponsesModel = new ThreadResponsesModel(TheradResponsesCodeEnum.SUCCESS, "", null,
                    getTablesCreateSqlList(connection, dname, tnameList));
            connection.close();
        } catch (Exception e) {
            log.error(e.getLocalizedMessage());
            threadResponsesModel = new ThreadResponsesModel(TheradResponsesCodeEnum.ERROR, e.getMessage(), tnameList, null);
        }
        return threadResponsesModel;
    }
}
