package com.example.logicbackfrontback.model.poolConfigModel;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
public class ThreadPoolConfig {
    @Value("${task.pool.initCorePoolSize}")
    private int initCorePoolSize;
    @Value("${task.pool.initMaxPoolSize}")
    private int initMaxPoolSize;
    @Value("${task.pool.initKeepAliveSeconds}")
    private int initKeepAliveSeconds;
    @Value("${task.pool.initQueueCapacity}")
    private int initQueueCapacity;
    @Value("${task.pool.minSetCorePoolSize}")
    private int minSetCorePoolSize;
    @Value("${task.pool.minSetMaxPoolSize}")
    private int minSetMaxPoolSize;
    @Value("${task.pool.maxSetCorePoolSize}")
    private int maxSetCorePoolSize;
    @Value("${task.pool.maxSetMaxPoolSize}")
    private int maxSetMaxPoolSize;
    @Value("${task.pool.addPoolSize}")
    private int addPoolSize;
    @Value("${task.pool.updateErrorRate}")
    private float updateErrorRate;
    @Value("${task.pool.addRightStep}")
    private int addRightStep;
}
