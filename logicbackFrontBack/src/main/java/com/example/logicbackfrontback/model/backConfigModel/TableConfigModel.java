package com.example.logicbackfrontback.model.backConfigModel;

import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.List;
@Getter
@Setter
public class TableConfigModel {
    //表的姓名
    private String name;
    //如果填了各个字段
    private List<String> columnNameList;

    public TableConfigModel(String name, List<String> columnNameList ) {
        this.columnNameList = columnNameList;
        this.name = name;
    }
}
