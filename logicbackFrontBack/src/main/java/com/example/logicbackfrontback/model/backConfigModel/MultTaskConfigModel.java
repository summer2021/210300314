package com.example.logicbackfrontback.model.backConfigModel;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
public class MultTaskConfigModel {
    /**
     * 一个任务有多少条sql语句
     */
    @Value("${multtaskmodel.schemaTaskSqlMount}")
    private int schemaTaskSqlMount;
    @Value("${multtaskmodel.queryDataCount}")
    private int queryDataCount;
    @Value("${multtaskmodel.queryDataRepeatCount}")
    private int queryDataRepeatCount;

}
