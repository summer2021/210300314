package com.example.logicbackfrontback.model.poolConfigModel;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.*;

@Slf4j
public class BspMultTheradPool {
    private final ThreadPoolExecutor threadPoolExecutor ;
    private final ThreadPoolConfig poolConfig;
    private int addRightStep;
    public ThreadPoolExecutor getThreadPoolExecutor() {
        return threadPoolExecutor;
    }

    public BspMultTheradPool(ThreadPoolConfig poolConfig) {
        this.addRightStep=1;
        this.poolConfig=poolConfig;
        this.threadPoolExecutor=new ThreadPoolExecutor(poolConfig.getInitCorePoolSize(),poolConfig.getInitMaxPoolSize(),
                poolConfig.getInitKeepAliveSeconds(),TimeUnit.SECONDS,new LinkedBlockingQueue<>(poolConfig.getInitQueueCapacity())
                , new ThreadPoolExecutor.CallerRunsPolicy());
    }
    public void updateCoreAndMaxPoolSize(int errorCount)
    {
        int maxPoolSize=threadPoolExecutor.getMaximumPoolSize();
        if(errorCount==0)
        {
            if(addRightStep== poolConfig.getAddRightStep())
            {
                maxPoolSize+=poolConfig.getAddPoolSize();
                if(maxPoolSize>poolConfig.getMaxSetMaxPoolSize())
                {
                    return;
                }
                addRightStep=0;
            }else {
                addRightStep++;
                return;
            }
        }else {
            addRightStep=0;
            //失败率较低的时候
            if(errorCount<maxPoolSize*poolConfig.getUpdateErrorRate())
            {
                return;
            }
            maxPoolSize=maxPoolSize-errorCount/2;
            if(maxPoolSize<poolConfig.getMinSetMaxPoolSize())
            {
                maxPoolSize=poolConfig.getMinSetMaxPoolSize();
            }
        }
        //设定最大连接池
        setMaximumPoolSize(maxPoolSize);
        System.out.println("maxPoolSize:"+maxPoolSize);
        //设置核心
        int corePoolSize=maxPoolSize/2;
        if(corePoolSize>poolConfig.getMaxSetCorePoolSize())
        {
            return;
        }
        if(corePoolSize<poolConfig.getMinSetCorePoolSize())
        {
            corePoolSize=poolConfig.getMinSetCorePoolSize();
        }
        setCorePoolSize(corePoolSize);
    }


    public void setCorePoolSize(int corePoolSize){
        try{
            threadPoolExecutor.setCorePoolSize(corePoolSize);
        }catch (Exception e){
            //设置失败
           log.error(e.getMessage());
        }
    }

    public int getCorePoolSize()
    {
        return threadPoolExecutor.getCorePoolSize();
    }

    public void setMaximumPoolSize(int maximumPoolSize) {
        try{
            threadPoolExecutor.setMaximumPoolSize(maximumPoolSize);
        }catch (Exception e){
            //设置失败
           log.error(e.getMessage());

        }
    }

    public int getMaximumPoolSize()
    {
        return threadPoolExecutor.getMaximumPoolSize();
    }

    public void execute(Runnable task) {
        threadPoolExecutor.execute(task);
    }


    public Future<?> submit(Runnable task) {
        return threadPoolExecutor.submit(task);
    }


    public <T> Future<T> submit(Callable<T> task) {
        return this.threadPoolExecutor.submit(task);
    }
    public void close()
    {
        threadPoolExecutor.shutdown();
    }
}
