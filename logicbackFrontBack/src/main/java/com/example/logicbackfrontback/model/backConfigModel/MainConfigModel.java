package com.example.logicbackfrontback.model.backConfigModel;

import com.alibaba.druid.pool.DruidDataSource;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
public class MainConfigModel implements Cloneable {
    //存储配置的bean
    private DataBaseConfigModel dataBaseConfigModel;

    //一些高级选项 例如保存的类型 保存的文件数量 保存文件的地址
    @Autowired
    private AdvanceOptionModel advanceOptionModel;

    //多线程的配置
    @Autowired
    private MultTaskConfigModel multTaskConfigModel;

    //阿里连接源
    private DruidDataSource dataSource;
}
