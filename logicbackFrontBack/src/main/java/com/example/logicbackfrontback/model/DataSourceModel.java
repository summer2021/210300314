package com.example.logicbackfrontback.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author jeffery
 * @date 2021/07/05
 * @desc 这个文件是数据源的一些配置项
 */
@Component
@Getter
@Setter
public class DataSourceModel {
    @Value("${datasource.driverClassName}")
    private String driverClassName;
    @Value("${datasource.url}")
    private String url;
    @Value("${datasource.username}")
    private String username;
    @Value("${datasource.password}")
    private String password;
    @Value("${datasource.initialSize}")
    private Integer initialSize;
    @Value("${datasource.minIdle}")
    private Integer minIdle;
    @Value("${datasource.connectionErrorRetryAmount}")
    private Integer connectionErrorRetryAmount;
    @Value("${datasource.maxActive}")
    private Integer maxActive;
    @Value("${datasource.maxWait}")
    private Integer maxWait;
    @Value("${datasource.validationQuery}")
    private String validationQuery;

}
