package com.example.logicbackfrontback.model.backConfigModel;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

@Getter
@Setter
public class DataBaseConfigModel {
    //数据库名
    private String name;
    //超级表集合
    private HashSet<String> stableHashSet;

    //表的集合
    private HashSet<String> tableHashSet;

    //带字段的表的集合
    private List<TableConfigModel> tableConfigModelList;

    public DataBaseConfigModel() {
        stableHashSet = new HashSet<>();
        tableHashSet = new HashSet<>();
        tableConfigModelList = new ArrayList<>();
    }
}
