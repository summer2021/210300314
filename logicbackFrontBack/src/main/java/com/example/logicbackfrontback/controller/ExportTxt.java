package com.example.logicbackfrontback.controller;

import com.example.logicbackfrontback.model.webSocket.WebSocket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ExportTxt {

      @Autowired
      private WebSocket websocket;

      @RequestMapping(value = "/test", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
      public void test(){
            String msg = "";
            int a = 0;
            for(int i=0;i<6;i++){
                  msg = String.valueOf(a);
                  websocket.sendAllMessage(msg);
                  a=a+20;
            }
      }
}
