package com.example.logicbackfrontback.controller;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.fastjson.JSONObject;
import com.example.logicbackfrontback.datasource.DataSourceConnectionFactory;
import com.example.logicbackfrontback.service.ReadConfigService;
import com.example.logicbackfrontback.service.ShowTDengineService;
import com.example.logicbackfrontback.util.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Connection;

@RestController
@RequestMapping("/objectSelector")
@Slf4j
public class ObjectSelectorController {
    @Autowired
    private ShowTDengineService showTDengineService;
    @Autowired
    private ReadConfigService readConfigService;
    @PostMapping("/getDatabaseName")
    public JSONObject getDatabaseName(@RequestBody JSONObject jsonObject)
    {
        JSONObject connConfigJson = jsonObject.getJSONObject("connConfig");
        DruidDataSource dataSource= readConfigService.getDatasourceFromConfig(connConfigJson);
        Connection connection=DataSourceConnectionFactory.createConnnection(dataSource);
        Object res=showTDengineService.getDataBaseName(connection);
        return CommonUtil.successJson(res);
    }
    @PostMapping("/getStableName")
    public JSONObject geStableName(@RequestBody JSONObject jsonObject)
    {
        String dname=jsonObject.getString("dname");
        JSONObject connConfigJson = jsonObject.getJSONObject("connConfig");
        DruidDataSource dataSource= readConfigService.getDatasourceFromConfig(connConfigJson);
        Connection connection=DataSourceConnectionFactory.createConnnection(dataSource);
        Object res=showTDengineService.getStableName(connection,dname);
        return CommonUtil.successJson(res);
    }
    @PostMapping("/getTableName")
    public JSONObject getTableName(@RequestBody JSONObject jsonObject)
    {
        String dname=jsonObject.getString("dname");
        JSONObject connConfigJson = jsonObject.getJSONObject("connConfig");
        DruidDataSource dataSource= readConfigService.getDatasourceFromConfig(connConfigJson);
        Connection connection=DataSourceConnectionFactory.createConnnection(dataSource);
        Object res=showTDengineService.getTableMessage(connection,dname);
        return CommonUtil.successJson(res);
    }
    @PostMapping("/getTableColumnName")
    public JSONObject getTableColumnName(@RequestBody JSONObject jsonObject)
    {
        String dname=jsonObject.getString("dname");
        String tname=jsonObject.getString("tname");
        JSONObject connConfigJson = jsonObject.getJSONObject("connConfig");
        DruidDataSource dataSource= readConfigService.getDatasourceFromConfig(connConfigJson);
        Connection connection=DataSourceConnectionFactory.createConnnection(dataSource);
        Object res=showTDengineService.getTableColumnName(connection,dname,tname);
        return CommonUtil.successJson(res);
    }
}
