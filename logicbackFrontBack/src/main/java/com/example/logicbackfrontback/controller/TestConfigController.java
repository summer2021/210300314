package com.example.logicbackfrontback.controller;

import com.alibaba.fastjson.JSONObject;
import com.example.logicbackfrontback.model.DataSourceModel;
import com.example.logicbackfrontback.service.IsRightConfigService;
import com.example.logicbackfrontback.util.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/testConfigController")
@Slf4j
public class TestConfigController {
    @Autowired
    IsRightConfigService isRightConfigService;
    @PostMapping("/testConnection")
    public JSONObject testConnection(@RequestBody JSONObject jsonObject)
    {
        String url=jsonObject.getString("url");
        String username=jsonObject.getString("username");
        String password=jsonObject.getString("password");
        Object res=isRightConfigService.isRightConnection(url,username,password);
        return CommonUtil.successJson(res);
    }
    @PostMapping("/testSaveDir")
    public JSONObject testSaveDir(@RequestBody JSONObject jsonObject)
    {
        String filename=jsonObject.getString("filename");
        Object res=isRightConfigService.isRightSaveDir(filename);
        return CommonUtil.successJson(res);
    }
}
