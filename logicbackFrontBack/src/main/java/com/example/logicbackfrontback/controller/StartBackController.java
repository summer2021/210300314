package com.example.logicbackfrontback.controller;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.fastjson.JSONObject;
import com.example.logicbackfrontback.datasource.DataSourceConnectionFactory;
import com.example.logicbackfrontback.model.backConfigModel.AdvanceOptionModel;
import com.example.logicbackfrontback.model.backConfigModel.DataBaseConfigModel;
import com.example.logicbackfrontback.model.backConfigModel.MainConfigModel;
import com.example.logicbackfrontback.service.LogicBackMainService;
import com.example.logicbackfrontback.service.ReadConfigService;
import com.example.logicbackfrontback.util.ChangeUtil;
import com.example.logicbackfrontback.util.CommonUtil;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.units.qual.C;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/startBack")
@Slf4j
public class StartBackController {
    @Autowired
    private MainConfigModel mainConfigModel;
    @Autowired
    private ReadConfigService readConfigService;
    @Autowired
    private LogicBackMainService logicBackMainService;
    @SneakyThrows
    @PostMapping("/uploadData")
    public JSONObject uploadData(@RequestBody JSONObject jsonObject)
    {
        ChangeUtil changeUtil = new ChangeUtil();
        JSONObject connConfigJson = jsonObject.getJSONObject("connConfig");
        DruidDataSource dataSource=readConfigService.getDatasourceFromConfig(connConfigJson);
        JSONObject objectSelect = jsonObject.getJSONObject("objectSelect");
        JSONObject advanceOption = jsonObject.getJSONObject("advanceOption");
        System.out.println("dddddd");
        System.out.println(objectSelect);
        System.out.println(advanceOption);
        System.out.println(dataSource);
        DataBaseConfigModel dataBaseConfigModel = readConfigService.readDataBaseConfig(objectSelect,dataSource.getConnection());
        mainConfigModel.setDataSource(dataSource);
        AdvanceOptionModel advanceOptionModel = readConfigService.readAdvanceOption(advanceOption);
        mainConfigModel.setAdvanceOptionModel(advanceOptionModel);
        mainConfigModel.setDataBaseConfigModel(dataBaseConfigModel);
        System.out.println(mainConfigModel.toString());
        //开始备份
        logicBackMainService.logicBackMain(mainConfigModel);
        return CommonUtil.successJson();
    }
}
