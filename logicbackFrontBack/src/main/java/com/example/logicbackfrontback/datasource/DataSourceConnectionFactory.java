package com.example.logicbackfrontback.datasource;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.fastjson.JSONObject;
import com.example.logicbackfrontback.model.DataSourceModel;
import lombok.extern.slf4j.Slf4j;
import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.jar.JarEntry;

//使用这个简单工厂类创建连接
@Slf4j
public  class DataSourceConnectionFactory {
    public static HashMap<String,DruidDataSource> dataSourceHashMap=new HashMap<>();
    public static Connection createConnnection(DruidDataSource dataSource)
    {
        Connection connection = null; // get connection
        try {
            connection = dataSource.getConnection();
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return connection;
    }
    //创建DruidDataSource
    public static DruidDataSource getDataSource(DataSourceModel dataSourceModel)
    {
        DruidDataSource dataSource = new DruidDataSource();
        // jdbc properties
        dataSource.setDriverClassName(dataSourceModel.getDriverClassName());
        dataSource.setUrl(dataSourceModel.getUrl());
        dataSource.setUsername(dataSourceModel.getUsername());
        dataSource.setPassword(dataSourceModel.getPassword());
        // pool configurations
        dataSource.setInitialSize(dataSourceModel.getInitialSize());
        dataSource.setMinIdle(dataSourceModel.getMinIdle());
        dataSource.setMaxActive(dataSourceModel.getMaxActive());
        dataSource.setMaxWait(dataSourceModel.getMaxWait());
        dataSource.setValidationQuery(dataSourceModel.getValidationQuery());
        dataSource.setConnectionErrorRetryAttempts(dataSourceModel.getConnectionErrorRetryAmount());
        return dataSource;
    }
}
