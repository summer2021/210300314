package com.example.logicbackfrontback.util;

import java.util.ArrayList;
import java.util.List;

public class MapUtil {

   public List<List<String>> mapListToList(List<String> list,int amount)
    {
        List<List<String>> res=new ArrayList<>();
        int i;
        for(i=0;i<list.size()-amount;i+=amount)
        {
            res.add(list.subList(i,i+amount));
        }
        if(i<list.size())
        {
            res.add(list.subList(i,list.size()));
        }
        return res;
    }

}
