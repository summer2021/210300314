package com.example.logicbackfrontback.util;

public enum BackFileTypeEnum {
    //保存为一个文件
    ONEFILE(1),
    //保存多个文件
    MULTFILE(2),
    ERRORTYPE(999);
    private final int val;
    BackFileTypeEnum(Integer val) {
        this.val=val;
    }

    public int getVal() {
        return val;
    }
}
