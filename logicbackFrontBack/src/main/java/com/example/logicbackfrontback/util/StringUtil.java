package com.example.logicbackfrontback.util;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Slf4j
public class StringUtil {
    public List<String> splitTableRange(String s)
    {
        List<String> list=new ArrayList<>();
        String[] temp=s.split("-");
        int starti=getIntIndex(temp[0]);
        int endi=getIntIndex(temp[1]);
        String startString=temp[0].substring(0,starti);
        String endString=temp[1].substring(0,endi);
        try {
            int start= Integer.parseInt(temp[0].substring(starti));
            int end= Integer.parseInt(temp[1].substring(endi));
            for(;start<=end;start++)
            {
                list.add(startString+String.valueOf(start));
            }
        }catch (Exception ignored)
        {
            log.error(ignored.getMessage());
        }
        return list;
    }

    /**
     * 得到字符串后的int的Index
     * @param str
     * @return 不同的index
     */
    public int getIntIndex(String str)
    {
        int i=str.length()-1;
        for(;i>=0;i--)
        {
            char c=str.charAt(i);
            if(c>'9'||c<'0') {
                break;
            }
        }
        i++;
        //删除相同的0
        for(;i<str.length();i++)
        {
            if(str.charAt(i)!=0)
            {
                break;
            }
        }
        return i;
    }
    public String collectionToString(Collection collection)
    {
        StringBuilder string= new StringBuilder();
        string.append('(');
        for(Object object:collection)
        {
            string.append('\'').append(String.valueOf(object)).append("',");
        }
        string.deleteCharAt(string.length() - 1);
        return string.append(')').toString();
    }
    public String finshCompareRate(int haveFinish,int allAmount)
    {
        Double  d=  (double) haveFinish / (double) allAmount;
        java.text.NumberFormat percentFormat =java.text.NumberFormat.getPercentInstance();

        percentFormat.setMaximumFractionDigits(2); //最大小数位数

        percentFormat.setMaximumIntegerDigits(3);//最大整数位数

        percentFormat.setMinimumFractionDigits(2); //最小小数位数

        percentFormat.setMinimumIntegerDigits(1);//最小整数位数

        return percentFormat.format(d);//自动转换成百分比显示
    }

}
