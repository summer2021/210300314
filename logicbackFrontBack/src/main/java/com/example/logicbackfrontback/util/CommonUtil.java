package com.example.logicbackfrontback.util;

import com.alibaba.fastjson.JSONObject;
import com.example.logicbackfrontback.util.constants.Constants;
import com.example.logicbackfrontback.util.constants.ErrorEnum;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;

public class CommonUtil {

	/**
	 * 返回一个info为空对象的成功消息的json
	 */
	public static JSONObject successJson() {
		return successJson(new JSONObject());
	}


	/**
	 * 返回一个info为空对象的成功消息的json
	 */
	public static JSONObject dealPermissions(JSONObject requestJson){


		String permissions = requestJson.getString("permissions").replace("[", "").replace("]","").replace(" ","");
		requestJson.put("permissions",permissions);

		return requestJson;
	}

	/**
	 * 返回一个返回码为100的json
	 */
	public static JSONObject successJson(Object info) {
		JSONObject resultJson = new JSONObject();
		resultJson.put("code", Constants.SUCCESS_CODE);
		resultJson.put("msg", Constants.SUCCESS_MSG);
		resultJson.put("info", info);
		return resultJson;
	}

	/**
	 * 返回错误信息JSON
	 */
	public static JSONObject errorJson(ErrorEnum errorEnum) {
		JSONObject resultJson = new JSONObject();
		resultJson.put("code", errorEnum.getErrorCode());
		resultJson.put("msg", errorEnum.getErrorMsg());
		resultJson.put("info", new JSONObject());
		return resultJson;
	}

	/**
	 * 查询分页结果后的封装工具方法
	 *
	 * @param requestJson 请求参数json,此json在之前调用fillPageParam 方法时,已经将pageRow放入
	 * @param list        查询分页对象list
	 * @param totalCount  查询出记录的总条数
	 */
	public static JSONObject successPage(final JSONObject requestJson, List<JSONObject> list, int totalCount) {
		int pageRow = requestJson.getIntValue("pageRow");
		int totalPage = getPageCounts(pageRow, totalCount);
		JSONObject result = successJson();
		JSONObject info = new JSONObject();
		info.put("list", list);
		info.put("totalCount", totalCount);
		info.put("totalPage", totalPage);
		result.put("info", info);
		return result;
	}
	public static JSONObject successPage(List<JSONObject> list, int totalCount) {
		JSONObject result = successJson();
		JSONObject info = new JSONObject();
		info.put("list", list);
		info.put("totalCount", totalCount);
		result.put("info", info);
		return result;
	}

	/**
	 * 查询分页结果后的封装工具方法
	 *
	 * @param list 查询分页对象list
	 */
	public static JSONObject successPage(List<JSONObject> list) {
		JSONObject result = successJson();
		JSONObject info = new JSONObject();
		info.put("list", list);
		result.put("info", info);
		return result;
	}

	/**
	 * 获取总页数
	 *
	 * @param pageRow   每页行数
	 * @param itemCount 结果的总条数
	 */
	private static int getPageCounts(int pageRow, int itemCount) {
		if (itemCount == 0) {
			return 1;
		}
		return itemCount % pageRow > 0 ?
				itemCount / pageRow + 1 :
				itemCount / pageRow;
	}

	/**
	 * 在分页查询之前,为查询条件里加上分页参数
	 *
	 * @param paramObject    查询条件json
	 * @param defaultPageRow 默认的每页条数,即前端不传pageRow参数时的每页条数
	 */
	private static void fillPageParam(final JSONObject paramObject, int defaultPageRow) {
		int pageNum = paramObject.getIntValue("pageNum");
		pageNum = pageNum == 0 ? 1 : pageNum;
		int pageRow = paramObject.getIntValue("pageRow");
		pageRow = pageRow == 0 ? defaultPageRow : pageRow;
		paramObject.put("offSet", (pageNum - 1) * pageRow);
		paramObject.put("pageRow", pageRow);
		paramObject.put("pageNum", pageNum);
		long unitId = paramObject.getLongValue("unitId");
		paramObject.put("unitId", unitId);
		//删除此参数,防止前端传了这个参数,pageHelper分页插件检测到之后,拦截导致SQL错误
		paramObject.remove("pageSize");
	}

	/**
	 * 分页查询之前的处理参数
	 * 没有传pageRow参数时,默认每页10条.
	 */
	public static void fillPageParam(final JSONObject paramObject) {
		fillPageParam(paramObject, 10);
	}

	/**
	 * 切割"#" 将查询结果JSONObject 转化为List<JSONObject>
	 */

	public static List<JSONObject> splitStringToList(String temp){

		String[] splitList = temp.split("#");
		List<JSONObject> list =new ArrayList<>();
		for (Integer a = 0;a < splitList.length; a++){
			JSONObject oj = new JSONObject();
			oj.put("name",splitList[a]);
			list.add(oj);
		}

		return list;
	}

	/**
	 *
	 * @param array 总String filter需要过滤的string
	 * @return
	 */
	public static  String filterString(String array,String filter){
		String result = "";
		String[] splitList = array.split("#");
		List<String> stringList = new ArrayList<>(Arrays.asList(splitList));

		for (int i = 0,lenth =stringList.size();i<lenth;i++){
			if (stringList.get(i).equals(filter)) {
				stringList.remove(i);
				break;
			}
		}

		if (stringList.size()>0){
				for (int j = 0 ;j< stringList.size();j++){
					if (j == 0){
						result = stringList.get(j);
					}
					else result = result + "#" +stringList.get(j);
				}
		}
		return result;
	}

	/**
	 *
	 * @param array 总String filter需要过滤的string
	 * @return
	 */
	public static  String filterUpdateString(String array,String filter,String newName){
		String result = "";
		String[] splitList = array.split("#");
		List<String> stringList = new ArrayList<>(Arrays.asList(splitList));

		for (int i = 0,lenth =stringList.size();i<lenth;i++){
			if (stringList.get(i).equals(filter)) {
				stringList.set(i , newName);
				break;
			}
		}


		//拼接list
		if (stringList.size()>0){
			for (int j = 0 ;j< stringList.size();j++){
				if (j == 0){
					result = stringList.get(j);
				}
				else result = result + "#" +stringList.get(j);
			}
		}
		return result;
	}

}
