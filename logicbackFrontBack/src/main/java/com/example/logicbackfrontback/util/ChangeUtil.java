package com.example.logicbackfrontback.util;

import com.alibaba.fastjson.JSONObject;
import com.example.logicbackfrontback.model.backConfigModel.DataBaseConfigModel;
import com.example.logicbackfrontback.model.backConfigModel.MainConfigModel;

import java.util.List;

public class ChangeUtil {
    public DataBaseConfigModel changeJsonObjectToMainConfigModel(JSONObject jsonObject) {
        DataBaseConfigModel dataBaseConfigModel = new DataBaseConfigModel();
        String dataBaseName = jsonObject.getString("dataBaseName");
        List<String> stableList = jsonObject.getJSONArray("stableList").toJavaList(String.class);
        List<String> tableList = jsonObject.getJSONArray("tableSelect").toJavaList(String.class);
        dataBaseConfigModel.setName(dataBaseName);
        return dataBaseConfigModel;
    }
}
