package com.example.logicbackfrontback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LogicbackFrontBackApplication {

    public static void main(String[] args) {
        SpringApplication.run(LogicbackFrontBackApplication.class, args);
    }
}
