package com.example.logicbackfrontback.service;


import com.alibaba.druid.pool.DruidDataSource;
import com.example.logicbackfrontback.model.backConfigModel.MainConfigModel;

import java.sql.Connection;
import java.util.HashSet;

public interface IsRightConfigService {

    boolean isRightConnection(String url,String username,String password);
    //判断文件夹是否存在
    boolean isRightSaveDir(String fileName);
}
