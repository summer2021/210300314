package com.example.logicbackfrontback.service.impl;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.fastjson.JSONObject;
import com.example.logicbackfrontback.datasource.DataSourceConnectionFactory;
import com.example.logicbackfrontback.model.DataSourceModel;
import com.example.logicbackfrontback.model.backConfigModel.DataBaseConfigModel;
import com.example.logicbackfrontback.model.backConfigModel.MainConfigModel;
import com.example.logicbackfrontback.model.backConfigModel.TableConfigModel;
import com.example.logicbackfrontback.model.poolConfigModel.BspMultTheradPool;
import com.example.logicbackfrontback.model.poolConfigModel.ThreadPoolConfig;
import com.example.logicbackfrontback.model.poolConfigModel.ThreadResponsesModel;
import com.example.logicbackfrontback.model.webSocket.WebSocket;
import com.example.logicbackfrontback.service.LogicBackMainService;
import com.example.logicbackfrontback.service.ShowTDengineService;
import com.example.logicbackfrontback.task.GetTableDataTask;
import com.example.logicbackfrontback.task.GetTablesCreateSqlListTask;
import com.example.logicbackfrontback.util.*;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@Service
@Slf4j
public class LogicBackMainServiceImpl implements LogicBackMainService {
    @Autowired
    private WebSocket websocket;
    @Autowired
    private ShowTDengineService showTDengineService;

    private final StringUtil stringUtil =new StringUtil();
    private final MapUtil mapUtil = new MapUtil();
    @Autowired
    private ThreadPoolConfig threadPoolConfig;
    @Override
    public void logicBackMain(MainConfigModel mainConfigModel) {
        try {
            BackFileTypeEnum backFileTypeEnum = mainConfigModel.getAdvanceOptionModel().getBackFileType();
            BackTypeEnum backTypeEnum = mainConfigModel.getAdvanceOptionModel().getBackTypeEnum();
            List<TableConfigModel> tableConfigModelList=mainConfigModel.getDataBaseConfigModel().getTableConfigModelList();
            HashSet<String> tableHashSet=mainConfigModel.getDataBaseConfigModel().getTableHashSet();
            //开始进行逻辑备份
            websocket.sendAllMessage("开始进行逻辑备份");
            switch (backFileTypeEnum) {
                case ONEFILE:
                    switch (backTypeEnum) {
                        case DATA:
                            backDataOneFile(mainConfigModel, tableConfigModelList);
                            break;
                        case SCHEMA:
                            backSchemaOneFile(mainConfigModel, tableHashSet);
                            break;
                        case SCHEMADATA:
                            backSchemaOneFile(mainConfigModel, tableHashSet);
                            backDataOneFile(mainConfigModel, tableConfigModelList);
                            break;
                        case ERRORTYPE:
                            break;
                        default:
                            break;
                    }
                    break;
                case MULTFILE:
                    switch (backTypeEnum) {
                        case DATA:
                            backDataMultFile(mainConfigModel, tableConfigModelList);
                            break;
                        case SCHEMA:
                            backSchemaMultFile(mainConfigModel, tableHashSet);
                            break;
                        case SCHEMADATA:
                            backSchemaMultFile(mainConfigModel, tableHashSet);
                            backDataMultFile(mainConfigModel, tableConfigModelList);
                            break;
                        case ERRORTYPE:
                            break;
                        default:
                            break;
                    }
                    break;
                case ERRORTYPE:
                    break;
                default:
                    break;
            }
            websocket.sendAllMessage("逻辑备份结束");
        }catch (Exception e)
        {
            websocket.sendAllMessage("出现未知错误");
            e.printStackTrace();
            log.error(e.getMessage());
        }
    }


    @Override
    public void backSchemaOneFile(MainConfigModel mainConfigModel, HashSet<String> tableSet) throws Exception {
        String saveDir = mainConfigModel.getAdvanceOptionModel().getSaveDir();
        String dname = mainConfigModel.getDataBaseConfigModel().getName();
        FileWriteUtil fileWriteUtil = new FileWriteUtil(saveDir,  dname+ ".sql", true);
        backSchemaFile(mainConfigModel,tableSet,fileWriteUtil);
    }

    @Override
    public void backSchemaMultFile(MainConfigModel mainConfigModel, HashSet<String> tableSet) throws Exception {
        String saveDir = mainConfigModel.getAdvanceOptionModel().getSaveDir();
        FileWriteUtil fileWriteUtil = new FileWriteUtil(saveDir, "schema.sql", true);
        backSchemaFile(mainConfigModel,tableSet,fileWriteUtil);
    }

    @Override
    public void backDataOneFile(MainConfigModel mainConfigModel, List<TableConfigModel> tableConfigModelList) throws Exception {
        DataSource dataSource = mainConfigModel.getDataSource();
        String dname = mainConfigModel.getDataBaseConfigModel().getName();
        String saveDir = mainConfigModel.getAdvanceOptionModel().getSaveDir();
        websocket.sendAllMessage("开始备份时序");
        FileWriteUtil fileWriteUtil = new FileWriteUtil(saveDir,
                mainConfigModel.getDataBaseConfigModel().getName() + ".sql", false);
        BspMultTheradPool bspMultTheradTaskPool = new BspMultTheradPool(threadPoolConfig);
        //一个超步的运行任务是maxpoolsize的两倍数量
        int group = bspMultTheradTaskPool.getMaximumPoolSize() * 2;
        for (int i = 0; i < tableConfigModelList.size(); ) {
            List<Future> list = new ArrayList<>();
            for (int j = i; j < i + group && j < tableConfigModelList.size(); j++) {
                list.add(bspMultTheradTaskPool.submit(new GetTableDataTask(dataSource, fileWriteUtil
                        , dname, tableConfigModelList.get(j),mainConfigModel.getMultTaskConfigModel())));
            }
            i = i + group;
            //如果i是大于他
            if (i > tableConfigModelList.size()) {
                i = tableConfigModelList.size();
            }
            int errorPoint = 0;
            for (Future future : list) {
                ThreadResponsesModel threadResponsesModel = (ThreadResponsesModel) future.get();
                if (threadResponsesModel.getCode() == TheradResponsesCodeEnum.ERROR) {
                    errorPoint++;
                    TableConfigModel tempData = (TableConfigModel) threadResponsesModel.getData();
                    tableConfigModelList.add(tempData);
                }
            }
            bspMultTheradTaskPool.updateCoreAndMaxPoolSize(errorPoint);
            group = bspMultTheradTaskPool.getMaximumPoolSize() * 2;
            websocket.sendAllMessage("时序数据已完成了: "+stringUtil.finshCompareRate(i,tableConfigModelList.size()));
        }
        fileWriteUtil.close();
        websocket.sendAllMessage("时序数据查询结束");
        bspMultTheradTaskPool.close();
    }
    @Override
    public void backDataMultFile(MainConfigModel mainConfigModel, List<TableConfigModel> tableConfigModelList) throws Exception {
        DataSource dataSource = mainConfigModel.getDataSource();
        String dname = mainConfigModel.getDataBaseConfigModel().getName();
        String saveDir = mainConfigModel.getAdvanceOptionModel().getSaveDir();
        websocket.sendAllMessage("开始备份时序");
        BspMultTheradPool bspMultTheradTaskPool = new BspMultTheradPool(threadPoolConfig);
        //一个超步的运行任务是maxpoolsize的两倍数量
        int group = bspMultTheradTaskPool.getMaximumPoolSize() * 2;
        for (int i = 0; i < tableConfigModelList.size(); ) {
            List<Future> list = new ArrayList<>();
            for (int j = i; j < i + group && j < tableConfigModelList.size(); j++) {
                TableConfigModel tableConfigModel = tableConfigModelList.get(j);
                FileWriteUtil fileWriteUtil = new FileWriteUtil(saveDir,
                        tableConfigModel.getName() + ".sql", true);
                list.add(bspMultTheradTaskPool.submit(new GetTableDataTask(dataSource, fileWriteUtil
                        , dname, tableConfigModel,mainConfigModel.getMultTaskConfigModel())));

            }
            i = i + group;
            //如果i是大于他
            if (i > tableConfigModelList.size()) {
                i = tableConfigModelList.size();
            }
            int errorPoint = 0;
            for (Future future : list) {
                ThreadResponsesModel threadResponsesModel = (ThreadResponsesModel) future.get();
                if (threadResponsesModel.getCode() == TheradResponsesCodeEnum.ERROR) {
                    errorPoint++;
                    TableConfigModel tempData = (TableConfigModel) threadResponsesModel.getData();
                    tableConfigModelList.add(tempData);
                }
            }
            bspMultTheradTaskPool.updateCoreAndMaxPoolSize(errorPoint);
            group = bspMultTheradTaskPool.getMaximumPoolSize() * 2;

            websocket.sendAllMessage("时序数据已完成了: "+stringUtil.finshCompareRate(i,tableConfigModelList.size()));
        }
        websocket.sendAllMessage("时序数据查询结束");
        bspMultTheradTaskPool.close();
    }
    @Override
    public void backSchemaFile(MainConfigModel mainConfigModel,HashSet<String> tableSet,FileWriteUtil fileWriteUtil) throws Exception {
        websocket.sendAllMessage("开始查询结构数据");
        // 查询 数据库和超级表
        String dname = mainConfigModel.getDataBaseConfigModel().getName();
        DataBaseConfigModel dataBaseConfigModel = mainConfigModel.getDataBaseConfigModel();
        DruidDataSource dataSource = mainConfigModel.getDataSource();
        Connection connection = dataSource.getConnection();
        String dataBaseCreateSql = showTDengineService.getDataBaseCreateSql(connection, mainConfigModel.getDataBaseConfigModel().getName());
        List<String> stablesCreateSqlList = showTDengineService.getStablesCreateSql(connection, dataBaseConfigModel.getName(), dataBaseConfigModel.getStableHashSet());
        connection.close();
        //写入数据
        fileWriteUtil.writeFileString("#创建数据库", dataBaseCreateSql);
        fileWriteUtil.writeFileList("#创建超级表", stablesCreateSqlList);
        //查询普通表
        List<String> tableList = new ArrayList<>(tableSet);
        int sqlAmount = mainConfigModel.getMultTaskConfigModel().getSchemaTaskSqlMount();
        List<List<String>> taskDataList = mapUtil.mapListToList(tableList, sqlAmount);
        BspMultTheradPool bspMultTheradTaskPool = new BspMultTheradPool(threadPoolConfig);
        //一个超步的运行任务是maxpoolsize的两倍数量
        int group = bspMultTheradTaskPool.getMaximumPoolSize() * 2;
        for (int i = 0; i < taskDataList.size(); ) {
            List<Future> list = new ArrayList<>();
            for (int j = i; j < i + group && j < taskDataList.size(); j++) {
                list.add(bspMultTheradTaskPool.submit(new GetTablesCreateSqlListTask(
                        mainConfigModel.getDataSource(), dname, taskDataList.get(j)
                )));
            }
            i = i + group;
            //如果i是大于他
            if (i > taskDataList.size()) {
                i = taskDataList.size();
            }
            int errorPoint = 0;
            for (Future future : list) {
                ThreadResponsesModel threadResponsesModel = (ThreadResponsesModel) future.get();
                if (threadResponsesModel.getCode() == TheradResponsesCodeEnum.SUCCESS) {
                    List<String> tempRes = (List<String>) threadResponsesModel.getRes();
                    fileWriteUtil.writeFileList("#写入表", tempRes);
                } else {
                    errorPoint++;
                    List<String> tempData = (List<String>) threadResponsesModel.getData();
                    taskDataList.add(tempData);
                }
            }
            bspMultTheradTaskPool.updateCoreAndMaxPoolSize(errorPoint);
            group = bspMultTheradTaskPool.getMaximumPoolSize() * 2;
            websocket.sendAllMessage("结构数据已完成了: "+stringUtil.finshCompareRate(i,taskDataList.size()));
        }
        fileWriteUtil.close();
        websocket.sendAllMessage("结构数数据查询完毕");
        bspMultTheradTaskPool.close();
    }
}
