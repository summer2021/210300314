package com.example.logicbackfrontback.service;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.fastjson.JSONObject;
import com.example.logicbackfrontback.model.backConfigModel.AdvanceOptionModel;
import com.example.logicbackfrontback.model.backConfigModel.DataBaseConfigModel;
import com.example.logicbackfrontback.model.backConfigModel.TableConfigModel;
import com.example.logicbackfrontback.util.BackFileTypeEnum;
import com.example.logicbackfrontback.util.BackTypeEnum;

import java.sql.Connection;
import java.util.HashMap;
import java.util.List;


public interface ReadConfigService {
    DataBaseConfigModel readDataBaseConfig(JSONObject jsonObject, Connection connection);

    HashMap<String, List<String>> changeSelectColumnToMap(JSONObject jsonObject);

    void putMapToDataBaseConfigModel(HashMap<String, List<String>> listHashMap,DataBaseConfigModel dataBaseConfigModel);

    HashMap<String, List<String>> standardSelectTable(
            List<String> tableSelect, HashMap<String, List<String>> selectColumn,
            HashMap<String, String> tables, List<String> stableLIst);
    AdvanceOptionModel readAdvanceOption(JSONObject jsonObject);

    BackTypeEnum getbackTypeEnum(String str);
    BackFileTypeEnum getbackFileTypeEnum(String str);
    DruidDataSource getDatasourceFromConfig(JSONObject jsonObject);
}
