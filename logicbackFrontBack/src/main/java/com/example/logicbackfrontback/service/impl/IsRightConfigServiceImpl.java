package com.example.logicbackfrontback.service.impl;

import com.alibaba.druid.pool.DruidDataSource;
import com.example.logicbackfrontback.datasource.DataSourceConnectionFactory;
import com.example.logicbackfrontback.model.backConfigModel.MainConfigModel;
import com.example.logicbackfrontback.model.DataSourceModel;
import com.example.logicbackfrontback.service.IsRightConfigService;
import com.example.logicbackfrontback.service.ShowTDengineService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.sql.Connection;
import java.util.HashSet;

@Service
@Slf4j
public class IsRightConfigServiceImpl implements IsRightConfigService {
    @Autowired
    DataSourceModel dataSourceModel;
    @Autowired
    private ShowTDengineService showTDengineService;

    @Override
    public boolean isRightConnection(String url,String username,String password) {
        dataSourceModel.setUsername(username);
        dataSourceModel.setPassword(password);
        dataSourceModel.setUrl(url);
        try {
            DruidDataSource dataSource= DataSourceConnectionFactory.getDataSource(dataSourceModel); // get connection
            //测试连接时我们只重复连接3次
            dataSource.setConnectionErrorRetryAttempts(3);
            Connection connection=dataSource.getConnection();
            connection.close();
            //如果连接成功我们将重连的次数返回
            dataSource.setConnectionErrorRetryAttempts(dataSourceModel.getConnectionErrorRetryAmount());
            String mapid=url+username+password;
            DataSourceConnectionFactory.dataSourceHashMap.put(mapid,dataSource);
            return true;
        } catch (Exception e)
        {
            log.error(e.getMessage());
        }
        return false;
    }

    @Override
    public boolean isRightSaveDir(String fileName) {
        File file =new File(fileName);
        if  (!file.exists()  && !file.isDirectory()){
            boolean res = file.mkdirs();
            if(res)
            {
                //删除文件夹
                boolean deleteRes=file.delete();
            }
            return res;
        }
        return true;
    }
}
