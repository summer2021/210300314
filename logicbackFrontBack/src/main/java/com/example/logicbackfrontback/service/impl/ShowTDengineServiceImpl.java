package com.example.logicbackfrontback.service.impl;
import com.example.logicbackfrontback.service.ShowTDengineService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

@Slf4j
@Service
public class ShowTDengineServiceImpl implements ShowTDengineService {
    @Override
    public HashSet<String> getDataBaseName(Connection connection) {
        HashSet<String> hashSet=new HashSet<>();
        try {
            Statement statement = connection.createStatement(); // get statement
            ResultSet resultSet = statement.executeQuery("show databases;");
            while (resultSet.next()) {
                hashSet.add(resultSet.getString("name"));
            }
        } catch (SQLException throwables) {
            log.error(throwables.getMessage());
        }
        return hashSet;
    }

    @Override
    public HashSet<String> getStableName(Connection connection, String dname) {
        HashSet<String> hashSet=new HashSet<>();
        try {
            Statement statement = connection.createStatement(); // get statement
            statement.execute("use "+dname+";");
            ResultSet resultSet = statement.executeQuery("show stables;");
            while (resultSet.next()) {
                hashSet.add(resultSet.getString("name"));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            log.error(throwables.getMessage());
        }
        return hashSet;
    }

    @Override
    public HashMap<String,String> getTableMessage(Connection connection, String dname) {
        HashMap<String,String> hashMap=new HashMap<>();
        try {
            Statement statement = connection.createStatement(); // get statement
            statement.execute("use "+dname+";");
            ResultSet resultSet = statement.executeQuery("show tables;");
            while (resultSet.next()) {
                hashMap.put(resultSet.getString("table_name"),resultSet.getString("stable_name"));
            }

        } catch (SQLException throwables) {
            log.error(throwables.getMessage());
        }
        return hashMap;
    }

    @Override
    public HashMap<String,List<String>> getTableName(Connection connection, String dname) {
        HashMap<String,List<String>> hashMap=new HashMap<>();
        try {
            Statement statement = connection.createStatement(); // get statement
            statement.execute("use "+dname+";");
            ResultSet resultSet = statement.executeQuery("show tables;");
            while (resultSet.next()) {
                String stableName=resultSet.getString("stable_name");
                String tableName=resultSet.getString("table_name");
                if(hashMap.containsKey(stableName))
                {
                    hashMap.get(stableName).add(tableName);
                }else {
                    List<String> list=new ArrayList<>();
                    list.add(tableName);
                    hashMap.put(stableName,list);
                }
            }
        } catch (SQLException throwables) {
            log.error(throwables.getMessage());
        }
        return hashMap;
    }

    @Override
    public List<String> getTableColumnName(Connection connection, String dname, String tname) {
        List<String> list=new ArrayList<>();
        try {
            Statement statement = connection.createStatement(); // get statement
            statement.execute("use "+dname+";");
            ResultSet resultSet = statement.executeQuery(" describe "+tname+";");
            while (resultSet.next()) {
                if("TAG".equals(resultSet.getString("Note")))
                {
                    continue;
                }
                list.add(resultSet.getString("Field"));
            }

        } catch (SQLException throwables) {
            log.error(throwables.getMessage());
        }
        return list;
    }

    @Override
    public String getDataBaseCreateSql(Connection connection, String name) {
        String res="";
        try {
            Statement statement = connection.createStatement(); // get statement
            ResultSet resultSet = statement.executeQuery("SHOW CREATE DATABASE "+name+";");
            if(resultSet.next())
            {
                res=resultSet.getString("Create Database");
                // 在res 中增加 IF NOT EXISTS
                return res.substring(0,15)+" IF NOT EXISTS"+res.substring(15);
            }
        } catch (SQLException throwables) {
            log.error(throwables.getMessage());
        }
        return res;
    }

    @Override
    public String getStableCreateSql(Connection connection, String dname, String sname) {
        String res="";
        try {
            Statement statement = connection.createStatement(); // get statement
            statement.execute("use "+dname+";");
            ResultSet resultSet = statement.executeQuery("SHOW CREATE STABLE "+sname+";");
            if (resultSet.next())
            {
                res=resultSet.getString("Create Table");
                return res.substring(0,13)+" IF NOT EXISTS"+res.substring(13);
            }

        } catch (SQLException throwables) {
            log.error(throwables.getMessage());
        }
        // 在res 中增加 IF NOT EXISTS
       return res;
    }

    @Override
    public String getTableCreateSql(Connection connection, String dname, String tname) {
        String res="";
        try {
            Statement statement = connection.createStatement(); // get statement
            statement.execute("use "+dname+";");
            ResultSet resultSet = statement.executeQuery("SHOW CREATE TABLE "+tname+";");
            if(resultSet.next())
            {
                res=resultSet.getString("Create Table");
                return res.substring(0,12)+" IF NOT EXISTS"+res.substring(12);
            }
        } catch (SQLException throwables) {
            log.error(throwables.getMessage());
        }
        // 在res 中增加 IF NOT EXISTS
        return res;
    }

    @Override
    public List<String> getStablesCreateSql(Connection connection, String dname, HashSet<String> snameSet) {
        List<String> res=new ArrayList<>();
        try {
            Statement statement = connection.createStatement(); // get statement
            statement.execute("use "+dname+";");
            for(String s: snameSet)
            {
                ResultSet resultSet = statement.executeQuery("SHOW CREATE STABLE "+s+";");
                if(resultSet.next())
                {
                    String temp=resultSet.getString("Create Table");
                    res.add( temp.substring(0,13)+" IF NOT EXISTS"+temp.substring(13));
                }
            }
        } catch (SQLException throwables) {
            log.error(throwables.getMessage());
        }
        return res;
    }

    @Override
    public List<String> getTablesCreateSqlList(Connection connection, String dname, List<String> tnameList) {
        List<String> res=new ArrayList<>();
        try {
            Statement statement = connection.createStatement(); // get statement
            statement.execute("use "+dname+";");
            for(String s: tnameList)
            {
                ResultSet resultSet = statement.executeQuery("SHOW CREATE TABLE "+s+";");
                if(resultSet.next())
                {
                    String temp=resultSet.getString("Create Table");
                    res.add( temp.substring(0,12)+" IF NOT EXISTS"+temp.substring(12));
                }
            }
        } catch (SQLException sqlException) {
            log.error(sqlException.getMessage());
            return null;
        }

        return res;
    }
}
