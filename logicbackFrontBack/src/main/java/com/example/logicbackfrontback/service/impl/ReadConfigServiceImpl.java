package com.example.logicbackfrontback.service.impl;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.fastjson.JSONObject;
import com.example.logicbackfrontback.datasource.DataSourceConnectionFactory;
import com.example.logicbackfrontback.model.DataSourceModel;
import com.example.logicbackfrontback.model.backConfigModel.AdvanceOptionModel;
import com.example.logicbackfrontback.model.backConfigModel.DataBaseConfigModel;
import com.example.logicbackfrontback.model.backConfigModel.TableConfigModel;
import com.example.logicbackfrontback.service.ReadConfigService;
import com.example.logicbackfrontback.service.ShowTDengineService;
import com.example.logicbackfrontback.util.BackFileTypeEnum;
import com.example.logicbackfrontback.util.BackTypeEnum;
import com.example.logicbackfrontback.util.StringUtil;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.util.*;

@Service
public class ReadConfigServiceImpl implements ReadConfigService {
    @Autowired
    private ShowTDengineService showTDengineService;
    @Autowired
    private DataSourceModel dataSourceModel;
    @Override
    public DataBaseConfigModel readDataBaseConfig(JSONObject jsonObject, Connection connection) {
        DataBaseConfigModel dataBaseConfigModel = new DataBaseConfigModel();
        //转数据库
        String dataBaseName = jsonObject.getString("dataBaseName");
        //转超级表
        List<String> stableList = jsonObject.getJSONArray("stableList").toJavaList(String.class);
        //转tableHashMap
        List<String> tableSelect = jsonObject.getJSONArray("tableSelect").toJavaList(String.class);
        JSONObject selectColumnJson = jsonObject.getJSONObject("tableSelectColumn");
        HashMap<String, List<String>> selectColumn = changeSelectColumnToMap(selectColumnJson);
        HashMap<String,String> tabless = showTDengineService.getTableMessage(connection,dataBaseName);
        HashMap<String, List<String>> tableHashMap = standardSelectTable(tableSelect,selectColumn,tabless,stableList);
        //存储
        dataBaseConfigModel.setName(dataBaseName);
        dataBaseConfigModel.setStableHashSet(new HashSet<>(stableList));
        putMapToDataBaseConfigModel(tableHashMap,dataBaseConfigModel);
        System.out.println(dataBaseConfigModel.getTableConfigModelList().size());
        System.out.println(dataBaseConfigModel.getTableHashSet().size());
        return dataBaseConfigModel;
    }

    @Override
    public HashMap<String, List<String>> changeSelectColumnToMap(JSONObject jsonObject) {
        HashMap<String, List<String>> hashMap = new HashMap<>();
        Set<String> keys = jsonObject.keySet();
        for (String key:keys) {
            List<String> columnList = jsonObject.getJSONArray(key).toJavaList(String.class);
            hashMap.put(key,columnList);
        }
        return hashMap;
    }

    @Override
    public void putMapToDataBaseConfigModel(HashMap<String, List<String>> listHashMap,DataBaseConfigModel dataBaseConfigModel) {
        List<TableConfigModel> list = dataBaseConfigModel.getTableConfigModelList();
        HashSet<String> stringHashSet = dataBaseConfigModel.getTableHashSet();
        for (String tableName:listHashMap.keySet()) {
            stringHashSet.add(tableName);
            list.add(new TableConfigModel(tableName,listHashMap.get(tableName)));
        }
    }

    @Override
    public HashMap<String, List<String>> standardSelectTable(List<String> tableSelect, HashMap<String,
            List<String>> selectColumn, HashMap<String, String> tables, List<String> stableLIst) {
        StringUtil stringUtil = new StringUtil();
        HashMap<String, List<String>> hashMap = new HashMap<>();
        for (String selectPart:tableSelect) {
            List<String> columnList = selectColumn.get(selectPart);
            if(selectPart.contains("-"))
            {
                List<String> tableList = stringUtil.splitTableRange(selectPart);
                String stableName = tables.get(tableList.get(0));
                //如果超级表没有加入那么我们增加
                if(stableName.length()>0) {
                    if(!stableLIst.contains(stableName)){
                        stableLIst.add(stableName);
                    }
                }

                for(String tableName: tableList)
                {
                    //如果存在该表，那么我们保存
                    if(tables.containsKey(tableName))
                    {
                        hashMap.put(tableName,columnList);
                    }
                }

            }else {
                String stableName = tables.get(selectPart);
                //如果超级表没有加入那么我们增加
                if(stableName.length()>0) {
                    if(!stableLIst.contains(stableName)){
                        stableLIst.add(stableName);
                    }
                }
                hashMap.put(selectPart,columnList);
            }
        }
        return hashMap;
    }

    @Override
    public AdvanceOptionModel readAdvanceOption(JSONObject jsonObject) {
        String backFileType = jsonObject.getString("backFileType");
        String backType = jsonObject.getString("backType");
        String saveDir = jsonObject.getString("saveDir");
        AdvanceOptionModel advanceOptionModel = new AdvanceOptionModel();
        BackFileTypeEnum backFileTypeEnum = getbackFileTypeEnum(backFileType);
        BackTypeEnum backTypeEnum = getbackTypeEnum(backType);
        advanceOptionModel.setBackFileType(backFileTypeEnum);
        advanceOptionModel.setBackTypeEnum(backTypeEnum);
        advanceOptionModel.setSaveDir(saveDir);
        return advanceOptionModel;
    }

    @Override
    public BackFileTypeEnum getbackFileTypeEnum(String str) {
        BackFileTypeEnum backFileTypeEnum;
        switch (str)
        {
            case "1":backFileTypeEnum=BackFileTypeEnum.ONEFILE;break;
            case "2":backFileTypeEnum=BackFileTypeEnum.MULTFILE;break;
            default:backFileTypeEnum=BackFileTypeEnum.ERRORTYPE;break;
        }
        return backFileTypeEnum;
    }

    @Override
    public BackTypeEnum getbackTypeEnum(String str) {
        BackTypeEnum backTypeEnum;
        switch (str)
        {
            case "1":backTypeEnum=BackTypeEnum.SCHEMA;break;
            case "2":backTypeEnum=BackTypeEnum.DATA;break;
            case "3":backTypeEnum=BackTypeEnum.SCHEMADATA;break;
            default:backTypeEnum=BackTypeEnum.ERRORTYPE;break;
        }
        return backTypeEnum;
    }

    @Override
    public DruidDataSource getDatasourceFromConfig(JSONObject jsonObject) {
        String url=jsonObject.getString("url");
        String username=jsonObject.getString("username");
        String password=jsonObject.getString("password");
        dataSourceModel.setUsername(username);
        dataSourceModel.setPassword(password);
        dataSourceModel.setUrl(url);
        String mapid=url+username+password;
        System.out.println(mapid);
        if(DataSourceConnectionFactory.dataSourceHashMap.containsKey(mapid)) {
            return DataSourceConnectionFactory.dataSourceHashMap.get(mapid);
        }
        DruidDataSource dataSource= DataSourceConnectionFactory.getDataSource(dataSourceModel);
        DataSourceConnectionFactory.dataSourceHashMap.put(mapid,dataSource);
        return dataSource;
    }
}
