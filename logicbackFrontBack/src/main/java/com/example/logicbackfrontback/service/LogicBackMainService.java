package com.example.logicbackfrontback.service;


import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.fastjson.JSONObject;
import com.example.logicbackfrontback.model.backConfigModel.MainConfigModel;
import com.example.logicbackfrontback.model.backConfigModel.TableConfigModel;
import com.example.logicbackfrontback.util.FileWriteUtil;

import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ExecutionException;

public interface LogicBackMainService {
    void logicBackMain(MainConfigModel mainConfigModel);
    void backSchemaOneFile(MainConfigModel mainConfigModel, HashSet<String> tableSet) throws Exception;
    void backSchemaMultFile(MainConfigModel mainConfigModel, HashSet<String> tableSet) throws Exception;
    void backDataOneFile(MainConfigModel mainConfigModel, List<TableConfigModel> tableConfigModelList) throws Exception;
    void backDataMultFile(MainConfigModel mainConfigModel,List<TableConfigModel> tableConfigModelList) throws Exception;
    public void backSchemaFile(MainConfigModel mainConfigModel, HashSet<String> tableSet, FileWriteUtil fileWriteUtil) throws Exception;

}
