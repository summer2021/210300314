# 环境：

## 安装TDengine

TDengine-server-2.1.3.2-Linux-x64 

TDengine-client-2.1.3.2-Linux-x64

TDengine-client-2.1.3.2-Windows-x64

## TAOS-JDBCDriver 版本

taos-jdbcdriver-2.0.31

## jdk版本

1.8

# clone

https://gitlab.summer-ospp.ac.cn/summer2021/210300314.git

# 安装logicback

## 用idea打开logicback

![image.png](https://cdn.nlark.com/yuque/0/2021/png/2573838/1631436111448-ed258561-87a2-4b95-a4a3-a6312f490468.png)

## 导入maven的依赖

![image.png](https://cdn.nlark.com/yuque/0/2021/png/2573838/1631436265197-cace40e6-c0af-4dfb-885f-52a647b3331e.png)

## 开始运行

![image.png](https://cdn.nlark.com/yuque/0/2021/png/2573838/1631436347298-b3e25c41-a5f7-4584-ae00-d7f0fafc2112.png)

![image.png](https://cdn.nlark.com/yuque/0/2021/png/2573838/1631436413713-5eee33eb-2b4e-4e65-9b98-8c33821b9819.png)

# 安装图形化界面的logicback

# 安装logicbackFront

![image.png](https://cdn.nlark.com/yuque/0/2021/png/2573838/1631437094463-fafae597-ba0b-4532-934d-4fa924448710.png)

## 安装npm

![image.png](https://cdn.nlark.com/yuque/0/2021/png/2573838/1631437407398-183fb7d4-3837-455c-bb2a-4f2efbad54a0.png)

## 打开package.json运行dev

![image.png](https://cdn.nlark.com/yuque/0/2021/png/2573838/1631438062841-1e8bdc36-18a8-4310-aa9d-b1ab712d1245.png?x-oss-process=image%2Fresize%2Cw_1500)

# 安装logicbackFrongBack

![image.png](https://cdn.nlark.com/yuque/0/2021/png/2573838/1631438193097-a2a77d0b-05d6-4394-b271-81722131e125.png)

## 导入maven的依赖

![image.png](https://cdn.nlark.com/yuque/0/2021/png/2573838/1631436265197-cace40e6-c0af-4dfb-885f-52a647b3331e.png)

## 开始运行

![image.png](https://cdn.nlark.com/yuque/0/2021/png/2573838/1631438304789-a4a2544e-6b16-438f-b522-f0fdd3c82f6b.png)